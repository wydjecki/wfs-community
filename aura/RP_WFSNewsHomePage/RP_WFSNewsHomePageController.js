({
	initialize : function(component, event, helper) {
		helper.getCurrentUser(component);
	}

	, gotoNewsArticle: function(component, event, helper) {
		'use strict';
		var newsId = event.currentTarget.getAttribute("data-recId");
		var recType = event.currentTarget.getAttribute("data-recType");
		var extURL = event.currentTarget.getAttribute("data-URL");
			if(recType == 'External'){
				window.open(extURL);
			}else {
			  location.href ='/customers/s/news/' + newsId;
		}
	}

	// , gotoNewsArticle: function(component, event, helper) {
	// 	'use strict';
	// 	var articleId = event.currentTarget.getAttribute("data-recId");
	// 	var isExternal = event.currentTarget.getAttribute("data-external");
	// 	var extURL = event.currentTarget.getAttribute("data-URL");
	// 		if(isExternal){
	// 			window.open(extURL);
	// 		}else {
	// 			window.open (('article/' + articleId), '_self');
	// 	}
	// }
})