({
    initialize: function(component, event, helper) {
			console.log('Initialized');
    }

	, gotoIdeas: function(component, event, helper) {
				var a = $A.get('e.force:navigateToURL');
				a.setParams({
					'url' : '/community-ideas'
				});
				a.fire();
	}
  , gotoDocs: function(component, event, helper) {
				var a = $A.get('e.force:navigateToURL');
				a.setParams({
					'url' : '/documents'
				});
				a.fire();
	}
  , gotoKnowledge: function(component, event, helper) {
				var a = $A.get('e.force:navigateToURL');
				a.setParams({
					'url' : '/topiccatalog'
				});
				a.fire();
	}
	// , goToPostIdea: function(component, event, helper) {
  //       var a = $A.get('e.force:navigateToURL');
  //       a.setParams({
  //       	'url' : '/community-idea-post'
  //       });
  //       a.fire();
	// }
	, goToCases: function(component, event, helper) {
			var a = $A.get('e.force:navigateToURL');
			a.setParams({
					'url' : '/case/Case/Recent'
			});
			a.fire();
	}
  //   , goToCaseCreation: function(component, event, helper) {
  //       var a = $A.get('e.force:navigateToURL');
  //       a.setParams({
  //           'url' : '/casecreation'
  //       });
  //       a.fire();
  // }
  //   , goToSubscription: function(component, event, helper) {
  //       var a = $A.get('e.force:navigateToURL');
  //       a.setParams({
  //           'url' : '/subscriptions'
  //       });
  //       a.fire();
  // }
		, goToDocuments: function(component, event, helper) {
				var a = $A.get('e.force:navigateToURL');
				a.setParams({
						'url' : '/casecreation'
				});
				a.fire();
		}
})