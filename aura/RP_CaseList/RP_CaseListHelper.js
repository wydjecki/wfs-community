({
	getMyCases: function(component) {
		var m = component.get('c.getMyOpenCases');
		m.setParams({
			sortField: 'CreatedDate'
			, sortDirection: 'DESC'
		});
		m.setCallback(this, function(response) {
			console.log('getMyCases: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.cases', response.getReturnValue());
			console.log('Cases: ' + JSON.stringify(response.getReturnValue()));
			}
		});
		$A.enqueueAction(m);
		
	}
})