({
  getCurrentUser: function(component) {
		var action = component.get('c.currentUserType');
		action.setCallback(this, function(response) {
					var userType = component.get("v.userType");
					var state = response.getState();
					console.log('RESPONSE: ' + response);
					if (state == 'SUCCESS') {
							component.set('v.userType', response.getReturnValue());
              this.getNews(component);
              this.getPicklistValues(component);
					} else if (state == 'ERROR') {
							console.log('ERROR: ' + JSON.stringify(response.getError()));
					}
			});
			$A.enqueueAction(action);
	},

  // // getPicklistValues: function(component) {
  //
  // },

  getNews: function(component) {
    console.log('Helper called');
    console.log(component.get("v.userType"));
    var pageSize = component.get("v.pageSize");
		var filter = component.get('v.filter');
		var action = component.get("c.getWFSNews");
    var totalSize = component.get("v.totalSize");
    var currentUser = component.get("v.userType")
		action.setParams({
			filter: filter
			, sortField: 'LastPublishedDate'
			, sortDirection: 'ASC'
      , currentUser: currentUser
		});
		action.setCallback(this, function(response)
		{
			var state = response.getState();
			if (component.isValid() && state === "SUCCESS")
			{
				console.log(response.getReturnValue());
				component.set('v.news', response.getReturnValue());
				component.set("v.totalSize", component.get("v.news").length);
        console.log('TotalSize = ' + component.get("v.news").length);
				component.set("v.start",0);
				component.set("v.end", pageSize-1);

				var paginationList = [];

				for(var i=0; i<pageSize; i++)
				{
					paginationList.push(response.getReturnValue()[i]);
				}
				component.set('v.paginationList', paginationList);
			}
		});
		$A.enqueueAction(action);
	}

  , goToNext : function(component, event, helper) {
			var news = component.get("v.news");
			var end = component.get("v.end");
			var start = component.get("v.start");
			var pageSize = component.get("v.pageSize");
			var paginationList = [];

			var counter = 0;
			for(var i=end+1; i<end+pageSize+1; i++)
			{
				if(news.length > end)
				{
					paginationList.push(news[i]);
					counter ++ ;
				}
			}
			start = start + counter;
			end = end + counter;

			component.set("v.start",start);
			component.set("v.end",end);

			component.set('v.paginationList', paginationList);
	}
  , goToPrevious : function(component, event, helper) {
			var news = component.get("v.news");
			var end = component.get("v.end");
			var start = component.get("v.start");
			var pageSize = component.get("v.pageSize");
			var paginationList = [];

			var counter = 0;
			for(var i = start-pageSize; i < start ; i++)
			{
				if(i > -1)
				{
					paginationList.push(news[i]);
					counter ++;
				}
				else
				{
					start++;
				}
			}
			start = start - counter;
			end = end - counter;

			component.set("v.start",start);
			component.set("v.end",end);

			component.set('v.paginationList', paginationList);
		}

})