({
	initialize : function(component, event, helper) {
		console.log('Initializing...');
		helper.getCurrentUser(component);

	}

	, next : function(component, event, helper) {
			helper.goToNext(component);
	}

	, previous : function(component, event, helper) {
			helper.goToPrevious(component);
		}

	, gotoNewsArticle: function(component, event, helper) {
		'use strict';
		var newsId = event.currentTarget.getAttribute("data-recId");
		var recType = event.currentTarget.getAttribute("data-recType");
		var extURL = event.currentTarget.getAttribute("data-URL");
			if(recType == 'External'){
				window.open(extURL);
			}else {
			  location.href ='/customers/s/news/' + newsId;
		}
	}
	, onSelectContent: function(component, event, helper) {
		'use strict';
			var contentType = component.find('contentType').get('v.value');
			component.set('v.filter', contentType);
			helper.getNews(component);
		}
	, showSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		$A.util.removeClass(s, 'slds-hide');
	}

	, hideSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	}
})