({
	initialize: function(component, event, helper) {
		console.log('INIT: ' + component.get('v.recordId'));

		helper.getCase(component);
	}
	, overLow: function(component, event, helper) {
		$A.util.toggleClass(component.find('low'), 'rp-highlight');
	}
	, outLow: function(component, event, helper) {
		$A.util.toggleClass(component.find('low'), 'rp-highlight');
	}
	, clickLow: function(component, event, helper) {
		console.log('CLICK LOW');
		var s = component.get('v.survey');
		s.Score__c = 25;
		$A.util.addClass(component.find('low'), 'rp-selected');
		$A.util.removeClass(component.find('medium'), 'rp-selected');
		$A.util.removeClass(component.find('high'), 'rp-selected');
	}
	, overMed: function(component, event, helper) {
		$A.util.toggleClass(component.find('medium'), 'rp-highlight');
	}
	, outMed: function(component, event, helper) {
		$A.util.toggleClass(component.find('medium'), 'rp-highlight');
	}
	, clickMed: function(component, event, helper) {
		console.log('CLICK MED');
		var s = component.get('v.survey');
		s.Score__c = 50;
		$A.util.addClass(component.find('medium'), 'rp-selected');
		$A.util.removeClass(component.find('low'), 'rp-selected');
		$A.util.removeClass(component.find('high'), 'rp-selected');
	}
	, overHigh: function(component, event, helper) {
		$A.util.toggleClass(component.find('high'), 'rp-highlight');
	}
	, outHigh: function(component, event, helper) {
		$A.util.toggleClass(component.find('high'), 'rp-highlight');
	}
	, clickHigh: function(component, event, helper) {
		console.log('CLICK HI');
		var s = component.get('v.survey');
		s.Score__c = 100;
		$A.util.addClass(component.find('high'), 'rp-selected');
		$A.util.removeClass(component.find('medium'), 'rp-selected');
		$A.util.removeClass(component.find('low'), 'rp-selected');
	}
	, pressSubmit: function(component, event, helper) {
		helper.submitSurvey(component);
	}
})