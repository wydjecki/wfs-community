({
	getCase: function(component) {
		console.log('REC ID: ' + component.get('v.recordId') + ' - CID: ' + component.get('v.cid'));
		var m = component.get('c.getSurvey');
		m.setParams({
			caseId: component.get('v.recordId')
			, contactId: component.get('v.cid')
		});
		m.setCallback(this, function(response) {
			console.log('getCase: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				var s = response.getReturnValue();
				component.set('v.survey', s);
				if (s.Status__c == 'Completed') {
					this.showConfirm(component);
				} else {
					this.showSurvey(component);
				}
				console.log('Survey: ' + JSON.stringify(s));
			} else {
				this.showError(component, 'Failed to load the survey.');
			}
		});
		$A.enqueueAction(m);
	}
	, getUser: function(component) {
		var m = component.get('c.getUser');
		m.setCallback(this, function(response) {
			console.log('getUserId: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				console.log('USER: ' + JSON.stringify(response.getReturnValue()));
			}
		});
		$A.enqueueAction(m);
	}
	, submitSurvey: function(component) {
		var m = component.get('c.saveSurvey');
		var s = component.get('v.survey');
		s.sobjectType = 'Case_Survey__c';
		m.setParams({
			survey: s
		});
		m.setCallback(this, function(response) {
			console.log('saveSurvey: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				this.showConfirm(component);
				console.log('Survey: ' + JSON.stringify(response.getReturnValue()));
			} else {
				this.showError(component, 'Failed to submit survey.');
			}
		});
		$A.enqueueAction(m);
	}
	, showSurvey: function(component, message) {
		$A.util.removeClass(component.find('survey'), 'slds-hide');
		if(!$A.util.hasClass(component.find('err'), 'slds-hide')) $A.util.addClass(component.find('err'), 'slds-hide');
		if(!$A.util.hasClass(component.find('confirm'), 'slds-hide')) $A.util.addClass(component.find('confirm'), 'slds-hide');
	}
	, showConfirm: function(component, message) {
		$A.util.removeClass(component.find('confirm'), 'slds-hide');
		if(!$A.util.hasClass(component.find('err'), 'slds-hide')) $A.util.addClass(component.find('err'), 'slds-hide');
		if(!$A.util.hasClass(component.find('survey'), 'slds-hide')) $A.util.addClass(component.find('survey'), 'slds-hide');
	}
	, showError: function(component, message) {
		component.set('v.errMsg', message);
		$A.util.removeClass(component.find('err'), 'slds-hide');
		if(!$A.util.hasClass(component.find('confirm'), 'slds-hide')) $A.util.addClass(component.find('confirm'), 'slds-hide');
		if(!$A.util.hasClass(component.find('survey'), 'slds-hide')) $A.util.addClass(component.find('survey'), 'slds-hide');

	}
})