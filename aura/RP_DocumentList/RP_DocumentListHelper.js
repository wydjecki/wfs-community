({
	executeSearch: function(component, filter) {
		this.showSpinner(component);
		//console.log('FILTER: ' + JSON.stringify(filter));
		component.set('v.filter', filter);

	    var m = component.get('c.search');
	    m.setParams({
	    	filter: JSON.stringify(filter)
	    });
	    m.setCallback(this, function(response) {
	    	//console.log('RESPONSE: ' + response.getReturnValue());
	    	var state = response.getState();
	    	this.hideSpinner(component);
	    	if (state == 'SUCCESS') {
	    		//console.log('STATE SUCCESS: ' + response.getReturnValue());
	    		var data = JSON.parse(response.getReturnValue());
	    		var docs = [];
	    		var rs = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
	    		for (var i = 0; i < rs.length; i++) {
	    			var r = rs[i];
	    			var doc = {};
	    			var cs = r.Cells.results;
	    			for (var j = 0; j < cs.length; j++) {
	    				var c = cs[j];
	    				doc[c.Key] = c.Value;
	        		}
	        		doc.Previewable = (doc['ServerRedirectedEmbedUrl'] && doc['FileType'] != 'pdf') ? true : false;
	        		//console.log('PREVIEW: ' + doc['ServerRedirectedEmbedUrl'] + ' -- ' + doc['FileType']);
	        		docs.push(doc);
	    		}
			    //console.log('DOCS: ' + JSON.stringify(docs));
	    		component.set('v.docs', docs);
	    		component.set('v.found', data.d.query.PrimaryQueryResult.RelevantResults.TotalRows);
	    		var page = component.get('v.page');
	    		var size = component.get('v.pageSize');
	    		var total = data.d.query.PrimaryQueryResult.RelevantResults.TotalRows;
	    		var count = data.d.query.PrimaryQueryResult.RelevantResults.RowCount;
	    		component.set('v.rowCount', count);
	    		//console.log('PAGINATION: ' + page + ' -- ' + size + ' -- ' + total + ' -- ' + count + ' -- ' + (total/size));
	    		if (count > 0) {
		    		if (page > 1) component.set('v.hasPrevious', true); else component.set('v.hasPrevious', false);
		    		if (page < (total / size)) component.set('v.hasNext', true); else component.set('v.hasNext', false);
		    		var row = (page - 1) * size;
		    		component.set('v.showing', 'Showing document(s): ' + ((count == 1) ? (row + 1) : (row + 1) + ' - ' + (row + count) ));
	    		} else {
	    			component.set('v.hasPrevious', false);
	    			component.set('v.hasNext', false);
	    			component.set('v.showing', '');
	    		}

	    		//component.get('v.page')
	    		//component.set('v.hasPrevious', data.d.query.PrimaryQueryResult.RelevantResults.TotalRows)

	    		var refs = data.d.query.PrimaryQueryResult.RefinementResults.Refiners.results;
	    		//console.log('REFS: ' + JSON.stringify(refs));
	    		for (var i = 0; i < refs.length; i++) {
	    			var items = [];
	    			//console.log('REF NAME: ' + refs[i].Name);
	    			if (refs[i].Name == 'RefinableCommunityPortalCategory') {
	    				var selected = component.get('v.selectedTopicRefiners');
	    				var ritems = refs[i].Entries.results;
	    				for (var j = 0; j < ritems.length; j++) {
	    					var s = ritems[j];
	    					s.Checked = (selected.indexOf(s.RefinementToken) >= 0);
	    					items.push(s);
	    				}
	    				component.set('v.topicRefiners', items);
	    				$A.util.removeClass(component.find('topic'), 'slds-hide')
	    			}
	    			if (refs[i].Name == 'RefinableCommunityProduct') {
	    				var selected = component.get('v.selectedProductRefiners');
	    				var ritems = refs[i].Entries.results;
	    				for (var j = 0; j < ritems.length; j++) {
	    					var s = ritems[j];
	    					s.Checked = (selected.indexOf(s.RefinementToken) >= 0);
	    					items.push(s);
	    				}
	    				component.set('v.productRefiners', items);
	    				$A.util.removeClass(component.find('product'), 'slds-hide')
	    			}
	    			if (refs[i].Name == 'RefinableCommunityAudience') {
	    				var selected = component.get('v.selectedAudienceRefiners');
	    				var ritems = refs[i].Entries.results;
	    				for (var j = 0; j < ritems.length; j++) {
	    					var s = ritems[j];
	    					s.Checked = (selected.indexOf(s.RefinementToken) >= 0);
	    					items.push(s);
	    				}
	    				component.set('v.audienceRefiners', items);
	    				$A.util.removeClass(component.find('audience'), 'slds-hide')
	    			}
	    			if (refs[i].Name == 'LastModifiedTime') {
	    				var selected = component.get('v.selectedDateRefiners');
	    				var ritems = refs[i].Entries.results;
	    				for (var j = 0; j < ritems.length; j++) {
	    					var s = ritems[j];
	    					var d = this.getDates(s.RefinementName);
	    					s.RefinementName = d;
	    					s.Checked = (selected.indexOf(s.RefinementToken) >= 0);
	    					items.push(ritems[j]);
	    				}
	    				component.set('v.dateRefiners', items);
	    				$A.util.removeClass(component.find('modifiedDate'), 'slds-hide')
	    			}
	    			if (refs[i].Name == 'FileType') {
	    				var selected = component.get('v.selectedTypeRefiners');
	    				var ritems = refs[i].Entries.results;
	    				for (var j = 0; j < ritems.length; j++) {
	    					var s = ritems[j];
	    					s.Checked = (selected.indexOf(s.RefinementToken) >= 0);
	    					items.push(s);
	    				}
	    				component.set('v.typeRefiners', items);
	    				$A.util.removeClass(component.find('fileType'), 'slds-hide')
	    			}
	    		}
	    	}
	    });
	    $A.enqueueAction(m);
	}

	, gotoDownload: function(component, filePath) {
		var m = component.get('c.downloadFile');
		m.setParams({
			path: filePath
		});
	    m.setCallback(this, function(response) {
	    	//console.log('RESPONSE: ' + response.getReturnValue());
	    	var state = response.getState();
	    	if (state == 'SUCCESS') {
	    		//console.log('DOWNLOAD SUCCESS: ' + response.getReturnValue());
	    		var jsonString = response.getReturnValue();
	    		//console.log('RES: ' + JSON.stringify(jsonString));
	    		var data = JSON.parse(jsonString);
	    		if (typeof data.anonymousDownloadUrl) {
	    			window.location.href = data.anonymousDownloadUrl;
	    		} else {
	    			alert('invalid download link');
	    		}
			} else {
				//console.log('ERROR: ' + JSON.stringify(response.getError()));
			}
		});
	    $A.enqueueAction(m);

	}
	, gotoPreview: function(component, filePath) {
		var m = component.get('c.getGuestAccessLink');
		m.setParams({
			path: filePath
		});
	    m.setCallback(this, function(response) {
	    	//console.log('RESPONSE: ' + response.getReturnValue());
	    	var state = response.getState();
	    	if (state == 'SUCCESS') {
	    		//console.log('PREVIEW SUCCESS: ' + response.getReturnValue());
	    		var jsonString = response.getReturnValue();
	    		//console.log('RES: ' + JSON.stringify(jsonString));

	    		var data = JSON.parse(jsonString);
	    		if (typeof data.anonymousGuestAccessUrl) {
	    			var g = $A.get('e.force:navigateToURL');
	    			g.setParams({
	    				'url' : data.anonymousGuestAccessUrl
	    			});
	    			g.fire();
	    		} else {
	    			alert('invalid preview link');
	    		}
			} else {
				//console.log('ERROR: ' + JSON.stringify(response.getError()));
			}
		});
	    $A.enqueueAction(m);

	}
	, showSpinner: function(component) {
    	var s = component.find('spinner');
    	$A.util.removeClass(s, 'slds-hide');
  	}

	, hideSpinner: function(component) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	}
	, buildFilter: function(component) {
		var refinement = '';

		var filters = [];
		var filter;
		var tokens;

		//topic filter
		filter = '';
		tokens = component.get('v.selectedTopicRefiners');
		if (tokens.length > 0) {
			filter = 'RefinableCommunityPortalCategory:';
			if (tokens.length == 1) filter += tokens[0];
			else filter += 'and(' + tokens.join() + ')';
			filters.push(filter);
		}
		//console.log('TOPIC FILTER: ' + filter);

		//product filter
		filter = '';
		tokens = component.get('v.selectedProductRefiners');
		if (tokens.length > 0) {
			filter = 'RefinableCommunityProduct:';
			if (tokens.length == 1) filter += tokens[0];
			else filter += 'and(' + tokens.join() + ')';
			filters.push(filter);
		}
		//console.log('PRODUCT FILTER: ' + filter);

		//modified time filter
		filter = '';
		tokens = component.get('v.selectedDateRefiners');
		if (tokens.length > 0) {
			filter = 'LastModifiedTime:';
			if (tokens.length == 1) filter += tokens[0];
			else filter += 'and(' + tokens.join() + ')';
			filters.push(filter);
		}
		//console.log('DATE FILTER: ' + filter);


		//file type filter
		filter = '';
		tokens = component.get('v.selectedTypeRefiners');
		if (tokens.length > 0) {
			filter = 'FileType:';
			if (tokens.length == 1) filter += tokens[0];
			else filter += 'and(' + tokens.join() + ')';
			filters.push(filter);
		}
		//console.log('FILE TYPE FILTER: ' + filter);

		if (filters.length > 0) {
			if (filters.length == 1) refinement = filters[0];
			else refinement = 'and(' + filters.join() + ')';
		}
		//console.log('REFINEMENT: ' + refinement);
		return refinement;
	}
	, getDates: function(str) {
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'
					, 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
					];
		var ds = str.match(/\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.\d{7}Z\b/g);
		if (ds) {
			for (var i = 0; i < ds.length; i++) {
				var d = new Date(ds[i]);
				var df = months[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear()
					+ ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()
				;
				str = str.replace(ds[i], df);
			}
		}
		return str;
	}
})