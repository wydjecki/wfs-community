({
	initialize: function(component, event, helper) {
		////console.log('KEYWORD: ' + component.get('v.keyword'));
		var filter = {};
		filter.keyword = component.get('v.keyword');
		filter.topicInfo = {};
		filter.topicInfo.Id = component.get('v.topicId');
		filter.topicInfo.Name = component.get('v.topicName');
		filter.pageInfo = {};
		filter.pageInfo.StartRow = (component.get('v.page') - 1) * component.get('v.pageSize');
		filter.pageInfo.RowLimit = component.get('v.pageSize');
		filter.refinements = {};
		helper.executeSearch(component, filter);
		//helper.runSearch(component);
	}
	, imageError: function(component, event, helper) {
		event.target.src =  $A.get('$Resource.Extension') + '/default.png';
	}
	, handleTopicFilter: function(component, event, helper) {
		var filter = component.get('v.filter');
		var ts = component.get('v.selectedTopicRefiners');
		var r = event.getSource();
		if (r.get('v.value')) {
			ts.push(r.get('v.text'));
		} else {
			var pos = -1;
			for (var i=0; i<ts.length; i++) {
				if (ts[i] == r.get('v.text')) pos = i;
			}
			if (pos > -1) ts.splice(pos, 1);
		}
		//console.log('SELECTED TOKENS: ' + ts);
		if (ts.length == 0) delete filter.refinements.Topic;
		else filter.refinements.Topic = ts;
		component.set('v.selectedTopicRefiners', ts);
		helper.executeSearch(component, filter);
	}
	, handleAudienceFilter: function(component, event, helper) {
		var filter = component.get('v.filter');
		//console.log('FILTER: ' + filter);
		var ts = component.get('v.selectedAudienceRefiners');
		var r = event.getSource();
		if (r.get('v.value')) {
			ts.push(r.get('v.text'));
		} else {
			var pos = -1;
			for (var i=0; i<ts.length; i++) {
				if (ts[i] == r.get('v.text')) pos = i;
			}
			if (pos > -1) ts.splice(pos, 1);
		}
		//console.log('SELECTED TOKENS: ' + ts);
		if (ts.length == 0) delete filter.refinements.Audience;
		else filter.refinements.Audience = ts;
		component.set('v.selectedAudienceRefiners', ts);
		helper.executeSearch(component, filter);
	}
	, handleProductFilter: function(component, event, helper) {
		var filter = component.get('v.filter');
		//console.log('FILTER: ' + filter);
		var ts = component.get('v.selectedProductRefiners');
		var r = event.getSource();
		if (r.get('v.value')) {
			ts.push(r.get('v.text'));
		} else {
			var pos = -1;
			for (var i=0; i<ts.length; i++) {
				if (ts[i] == r.get('v.text')) pos = i;
			}
			if (pos > -1) ts.splice(pos, 1);
		}
		//console.log('SELECTED TOKENS: ' + ts);
		if (ts.length == 0) delete filter.refinements.Product;
		else filter.refinements.Product = ts;
		component.set('v.selectedProductRefiners', ts);
		helper.executeSearch(component, filter);
	}
	, handleDateFilter: function(component, event, helper) {
		var filter = component.get('v.filter');
		var ts = component.get('v.selectedDateRefiners');
		var r = event.getSource();
		var selected = r.get('v.text');
		var range = '';

		//console.log('SELECTED: ' + selected);
		var d = new Date(); //console.log('TODAY: ' + d);
		var d1, d2;
		range += 'range(';
		if (selected == '+360') {
			d2 = new Date(d.getFullYear() - 1, d.getMonth(), d.getDate()); //console.log('D2: ' + d2);
			range += 'min,' 
				+ d2.getFullYear() 
				+ '-' 
				+ String('00' + (d2.getMonth() + 1)).slice(-2) 
				+ '-' 
				+ String('00' + d2.getDate()).slice(-2);

		} else if (selected == '180-360') {
			d1 = new Date(d.getFullYear() - 1, d.getMonth(), d.getDate()); //console.log('D1: ' + d1);
			d2 = new Date(d.getFullYear(), d.getMonth() - 6, d.getDate()); //console.log('D2: ' + d2);
			range += d1.getFullYear()
				+ '-'
				+ String('00' + (d1.getMonth() + 1)).slice(-2)
				+ '-' 
				+ String('00' + d1.getDate()).slice(-2)
			 	+ ',' 
				+ d2.getFullYear() 
				+ '-' 
				+ String('00' + (d2.getMonth() + 1)).slice(-2) 
				+ '-' 
				+ String('00' + d2.getDate()).slice(-2);
		} else if (selected == '30-180') {
			d1 = new Date(d.getFullYear(), d.getMonth() - 6, d.getDate()); //console.log('D1: ' + d2);
			d2 = new Date(d.getFullYear(), d.getMonth() - 1, d.getDate()); //console.log('D2: ' + d2);
			range += d1.getFullYear()
				+ '-'
				+ String('00' + (d1.getMonth() + 1)).slice(-2)
				+ '-' 
				+ String('00' + d1.getDate()).slice(-2)
			 	+ ',' 
				+ d2.getFullYear() 
				+ '-' 
				+ String('00' + (d2.getMonth() + 1)).slice(-2) 
				+ '-' 
				+ String('00' + d2.getDate()).slice(-2);
		} else if (selected == '-30') {
			d1 = new Date(d.getFullYear(), d.getMonth() - 1, d.getDate()); //console.log('D1: ' + d1);
			range += d1.getFullYear()
				+ '-'
				+ String('00' + (d1.getMonth() + 1)).slice(-2)
				+ '-' 
				+ String('00' + d1.getDate()).slice(-2)
			 	+ ',max';
		}
		range+= ')';
		//console.log('RANGE: ' + range);
		ts = [range];
		if (ts.length == 0) delete filter.refinements.ModifiedDate;
		else filter.refinements.ModifiedDate = ts;
		component.set('v.selectedDateRefiners', ts);
		helper.executeSearch(component, filter);
	}
	, handleTypeFilter: function(component, event, helper) {
		var filter = component.get('v.filter');
		var ts = component.get('v.selectedTypeRefiners');
		var r = event.getSource();
		if (r.get('v.value')) {
			ts.push(r.get('v.text'));
		} else {
			var pos = -1;
			for (var i=0; i<ts.length; i++) {
				if (ts[i] == r.get('v.text')) pos = i;
			}
			if (pos > -1) ts.splice(pos, 1);
		}
		//console.log('SELECTED TOKENS: ' + ts);
		if (ts.length == 0) delete filter.refinements.FileType;
		else filter.refinements.FileType = ts;
		component.set('v.selectedTypeRefiners', ts);
		helper.executeSearch(component, filter);
	}
	, handlePreview: function(component, event, helper) {
		var path = event.currentTarget.dataset.id;
		//console.log('PATH: ' + path);
		helper.gotoPreview(component, path);
	}
	, handleDownload: function(component, event, helper) {
		var path = event.currentTarget.dataset.id;
		//console.log('PATH: ' + path);
		helper.gotoDownload(component, path);
	}
	, handlePrevious: function(component, event, helper) {
		var filter = component.get('v.filter');
		var page = component.get('v.page');
		page -= 1;
		filter.pageInfo.StartRow = (page - 1) * component.get('v.pageSize');
		component.set('v.page', page);
		helper.executeSearch(component, filter);
	}
	, handleNext: function(component, event, helper) {
		var filter = component.get('v.filter');
		var page = component.get('v.page');
		page += 1;
		filter.pageInfo.StartRow = (page - 1) * component.get('v.pageSize');
		component.set('v.page', page);
		helper.executeSearch(component, filter);
	}

})