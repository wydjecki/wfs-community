({
	getIdeaList: function(component) {
		var keyword = component.get('v.keyword');
		var filter = component.get('v.filter');
		var page = component.get('v.page');
		var pageSize = component.get('v.pageSize')
		var startRow = (page-1) * pageSize;
		//console.log('PAGE: ' + page + ' -- SIZE: ' + pageSize + ' -- NEXT: ' + startRow);
		//console.log('KEYWORD: ' + keyword);
		//console.log('FILTER: ' + filter);
		var m = component.get('c.getIdeaList');
		m.setParams({
			keyword: keyword
			, statusFilter: filter
			, pageSize: pageSize
			, startRow: startRow
		});
		m.setCallback(this, function(response) {
			var state = response.getState();
			//console.log('getIdeaList: ' + JSON.stringify(response) + ' -- STATE: ' + state);
			if (state == 'SUCCESS') {
				var ideaList = response.getReturnValue();
				//console.log('IDEA LIST: ' + JSON.stringify(ideaList));
				////console.log('TOTAL: ' + ideaList.total);
				var count = ideaList.total; 
				var page = component.get('v.page');
				var pageSize = component.get('v.pageSize');
				//console.log('checkPages-- PAGE: ' + page + ' -- SIZE: ' + pageSize + ' -- COUNT: ' + count);
				component.set('v.hasNext', count > page * pageSize);
				component.set('v.hasPrevious', page > 1);

				var empty = component.find('emptyMessage');
				//console.log('EMPTY: ' + JSON.stringify(empty));
				if (count == 0) $A.util.removeClass(empty, 'slds-hide');
				else if (!$A.util.hasClass(empty, 'slds-hide')) $A.util.addClass(empty, 'slds-hide');


				component.set('v.ideaList', ideaList);


				//console.log('DONE UPDATE');
			} else if (state == 'ERROR') {
				//console.log('ERROR');
				this.showError(component, response.getError());
			} else {
				//console.log('getIdeas errors');
			} 
		});
		$A.enqueueAction(m); 
	}
	, getIdeas: function(component, start) {
		var keyword = component.get('v.keyword');
		var filter = component.get('v.filter');
		var pageSize = component.get('v.pageSize')
		//console.log('KEYWORD: ' + keyword);
		//console.log('FILTER: ' + filter);
		var m = component.get('c.getIdeas');
		m.setParams({
			keyword: keyword
			, statusFilter: filter
			, pageSize: pageSize
			, startRow: start
		});
		m.setCallback(this, function(response) {
			var state = response.getState();
			//console.log('getIdeas: ' + JSON.stringify(response) + ' -- STATE: ' + state);
			if (state == 'SUCCESS') {
				this.checkPages(component, keyword);
				component.set('v.ideas', response.getReturnValue()); 
			} else if (state == 'ERROR') {
				//console.log('ERROR');
				this.showError(component, response.getError());
			} else {
				//console.log('getIdeas errors');
			} 
		});
		$A.enqueueAction(m); 
	}
	
	, checkPages: function(component) {
		//console.log('checkPages: ' + keyword)
		var keyword = component.get('v.keyword');
		var filter = component.get('v.filter');
		//console.log('KEYWORD: ' + keyword);
		//console.log('FILTER: ' + filter);
		var m = component.get('c.getIdeasCount');
		m.setParams({
			keyword: keyword
			, statusFilter: filter
		});
		m.setCallback(this, function(response) {
			//console.log('hasMore: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				var count = response.getReturnValue(); 
				var page = component.get('v.page');
				var pageSize = component.get('v.pageSize');
				//console.log('checkPages-- PAGE: ' + page + ' -- SIZE: ' + pageSize + ' -- COUNT: ' + count);
				component.set('v.hasNext', count > page * pageSize);
				component.set('v.hasPrevious', page > 1);
			} else if (state == 'ERROR') {
				this.showError(component, response.getError());
			} else {
				//console.log('hasMore errors');
			} 
		});
		$A.enqueueAction(m); 
	}
	, voteIdea: function(component, ideaId, value) {
		var m = component.get('c.voteIdea');
		//console.log('ID: ' + ideaId + ' -- VAL: ' + value);
		m.setParams({
			ideaId: ideaId
			, value: value
		});
		m.setCallback(this, function(response) {
			//console.log('voteIdea: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				////console.log('voteIdea: ' + JSON.stringify(response.getReturnValue()));
				this.getIdeaList(component);
			} else if (state == 'ERROR') {
				this.showError(component, response.getError());
			} else {
				//console.log('voteIdea errors');
			}
		});
		$A.enqueueAction(m); 
	}

    , gotoDetail: function(component, id) {
    	//console.log('gotoDetail: ' + id);
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
        	'url' : '/community-idea?id=' + id
        });
        a.fire();
    }

    , showError: function(component, errors) {
    	//console.log('ERRORS: ' + JSON.stringify(errors));
    	if (errors) {
    		if (errors[0] && errors[0].message) {
		    	var t = $A.get("e.force:showToast");
		    	t.setParams({
		    		'title' : 'Error'
		    		, 'message' : errors[0].message
		    		, 'type' : 'error'
		    	});
		    	t.fire();
    		}
    	}
    }

})