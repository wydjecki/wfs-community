({
	initialize: function(component, event, helper) {
		component.set('v.page', 1);
		helper.getIdeaList(component);
	}
	, handleLike: function(component, event, helper) {
		var s = event.currentTarget;
		var id = s.dataset.id;
		//console.log('handleLike ID: ' + id);
		helper.voteIdea(component, id, 'Up');
	}
	, handleDislike: function(component, event, helper) {
		var s = event.currentTarget;
		var id = s.dataset.id;
		//console.log('handleDislike ID: ' + id);
		helper.voteIdea(component, id, 'Down');
	}
	, handleDetail: function(component, event, helper) {
		var s = event.currentTarget;
		var id = s.dataset.id;
		//console.log('handleDetail ID: ' + id);
		helper.gotoDetail(component, id);
	}
	, handleSearch: function(component, event, helper) {
		component.set('v.page', 1);
		helper.getIdeaList(component);
	}
	, handleAll: function(component, event, helper) {
		component.set('v.page', 1);
		component.set('v.filter', 'All');		
		helper.getIdeaList(component);
	}
	, handleDelivered: function(component, event, helper) {
		component.set('v.page', 1);
		component.set('v.filter', 'Delivered');		
		helper.getIdeaList(component);
	}
	, handleUndelivered: function(component, event, helper) {
		component.set('v.page', 1);
		component.set('v.filter', 'Undelivered');		
		helper.getIdeaList(component);
	}
	, handlePrevious: function(component, event, helper) {
		var page = component.get('v.page') - 1;
		component.set('v.page', page);
		//component.set('v.ideas', []);
		helper.getIdeaList(component);
	}	
	, handleNext: function(component, event, helper) {
		var page = component.get('v.page') + 1;
		component.set('v.page', page);
		//component.set('v.ideas', []);
		helper.getIdeaList(component);
	}	
})