({
	initialize : function(component, event, helper) {
		var c = {'sobjectType' : 'Case'};
		component.set('v.case', c);
		helper.getCaseFields(component);
		helper.getUsersAccount(component);
		helper.getPartnerAccountNames(component);
	},

	onAccountSelect : function(component, event, helper) {
		var pa = component.find("account").get("v.value");
		var usersAccount = component.get("v.usersAccount.Id");
		var contact = component.find("contactField");
		component.set("v.selectedAccount", pa);
		var selectedAccount = component.get("v.selectedAccount");
		if( selectedAccount != usersAccount || selectedAccount == null ){
			$A.util.removeClass(contact, 'slds-hide');
			helper.getAccContacts(component);
		}
			if( selectedAccount === usersAccount ) {
			component.set("v.contactId", null);
			$A.util.addClass(contact, 'slds-hide');

			}
	},

	onContactSelect : function(component, event, helper) {
		var con = component.find("contact").get("v.value");
		component.set("v.contactId", con);
	},

	 changeTopic: function(component, event, helper) {
		helper.populateTopicDependentPicklist(
			component
			, 'serviceArea'
			, 'Service_Area__c'
			, component.get('v.case.Case_Topic__c')
		);
	},

	 changeServiceArea: function(component, event, helper) {
		var serviceArea = component.get('v.case.Service_Area__c');
		var severity = component.find('severityField');
		if (serviceArea != 'WorkForce Time and Attendance') {
			$A.util.removeClass(severity, 'slds-hide');
		} else {
			if  (!$A.util.hasClass(severity, 'slds-hide')) $A.util.addClass(severity, 'slds-hide');
		}
		helper.populateTopicDependentPicklist(
			component
			, 'service'
			, 'Service__c'
			, serviceArea
		);
	},

	 changeService: function(component, event, helper) {
		helper.populateTopicDependentPicklist(
			component
			, 'request'
			, 'Request__c'
			, component.get('v.case.Service__c')
		);
	},

	changeType: function(component, event, helper) {
		helper.toggleMore(component);
	},

	 showSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		$A.util.removeClass(s, 'slds-hide');
	},

	hideSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	},

	 pressSubmit: function(component, event, helper) {
		var hasErrors = false;
		var type = component.find('type');
		var typeVal = type.get('v.value');
			if (typeVal == null ||  typeVal == '') {
				hasErrors = true;
				type.set('v.errors', [{message: 'Please select a value for the Type field'}]);
			} else {
				type.set('v.errors', null);
			}

		var sub = component.find('subject');
		var subVal = sub.get('v.value');
			if(subVal == null || subVal == '') {
				hasErrors = true;
				sub.set('v.errors', [{message: 'Please enter a value in the Subject field'}]);
			} else {
				sub.set('v.errors', null);
			}

		var desc = component.find('description');
		var descVal = desc.get('v.value');
			if(subVal == null || subVal == '') {
				hasErrors = true;
				desc.set('v.errors', [{message: 'Please enter a short description'}]);
			} else {
				desc.set('v.errors', null);
			}

		var servicearea = component.find('serviceArea');
		var serviceAreaValue = servicearea.get('v.value');
			if(serviceAreaValue == 'Pick a value...' || serviceAreaValue == '' || serviceAreaValue == null) {
				hasErrors = true;
				servicearea.set('v.errors', [{message: 'Please select a value'}]);
			} else {
				servicearea.set('v.errors', null);
			}

		var service = component.find('service');
		var serviceValue = service.get('v.value');
			if(serviceValue == 'Pick a value...' || serviceValue == '' || serviceValue == null) {
				hasErrors = true;
				service.set('v.errors', [{message: 'Please select a value'}]);
			} else {
				service.set('v.errors', null);
			}

			var req = component.find('request');
			var reqVal = req.get('v.value');
				if(reqVal == 'Pick a value...' || reqVal == '' || reqVal == null) {
					hasErrors = true;
					req.set('v.errors', [{message: 'Please select a value'}]);
				} else {
					req.set('v.errors', null);
				}

			var env = component.find('environment');
			var envVal = env.get('v.value');
				if(envVal == null || envVal == 'Pick an environment...') {
					hasErrors = true;
					env.set('v.errors', [{message: 'Please select an Environment'}]);
				} else {
					env.set('v.errors', null);
				}

			var systemDown = component.find('systemDown');
			var sysVal = systemDown.get('v.value');
				if(typeVal == 'Incident' && sysVal == null) {
					hasErrors = true;
					systemDown.set('v.errors', [{message: 'Please select Yes or No'}]);
				} else {
					systemDown.set('v.errors', null);
				}

			var imp = component.find('impact');
			var impVal = imp.get('v.value');
				if(typeVal == 'Incident' && impVal == null) {
					hasErrors = true;
					imp.set('v.errors', [{message: 'Please select Yes or No'}]);
				} else {
					imp.set('v.errors', null);
				}


		if (!hasErrors) helper.saveCase(component);
	}

})