({
	initialize: function(component, event, helper) {
		var url = window.location.search;
		var id = '';
		console.log('IDX: ' + url.indexOf('?'));
		var qs = url.substring(url.indexOf('?')+1);
		//console.log('SUBS: ' + qs);
		var tokens = qs.split('&');
		//console.log('TOKENS: ' + tokens);
		var idtoken = '';
		if (Array.isArray(tokens))  {
			for (var ctr = 0; ctr<tokens.length; ctr++) {
				//console.log('TOKEN: ' + tokens[ctr]);
				//console.log('ID= ' + tokens[ctr].indexOf('id='));
				if (tokens[ctr].indexOf('id=') == 0) {
					idtoken = tokens[ctr];
				}
			}
		} else idtoken = tokens;
		//console.log('IDTOKEN: ' + idtoken + ' - ' + idtoken.indexOf('='));
		if (idtoken.indexOf('=') > 0) {
			tokens = idtoken.split('=');
			id = tokens[1];
		}
		//console.log('IDEA ID: ' + id);
		component.set('v.ideaId', id);
		helper.getIdea(component, id);
		helper.getComments(component, id);
	}
	, handleComment: function(component, event, helper) {
		var id = component.get('v.ideaId');
		helper.setComment(component, id);

	}
	, handleLike: function(component, event, helper) {
		var id = component.get('v.ideaId');
		helper.voteIdea(component, id, 'Up');
	}
	, handleDislike: function(component, event, helper) {
		var id = component.get('v.ideaId');
		helper.voteIdea(component, id, 'Down');
	}
	, showSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		$A.util.removeClass(s, 'slds-hide');
	}

	, hideSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	}
})