({
	getIdea: function(component, id) {
		var m = component.get('c.getIdea');
		m.setParams({
			ideaId: id
		});
		m.setCallback(this, function(response) {
			//console.log('getIdea: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.idea', response.getReturnValue());
				//console.log('getIdea: ' + JSON.stringify(response.getReturnValue()));
			}
		});
		$A.enqueueAction(m);
	}

	, getComments: function(component, id) {
		var m = component.get('c.getComments');
		m.setParams({
			ideaId: id
		});
		m.setCallback(this, function(response) {
			//console.log('getIdea: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.comments', response.getReturnValue());
				//console.log('getComments: ' + JSON.stringify(response.getReturnValue()));
			}
		});
		$A.enqueueAction(m);
	}

	, setComment: function(component, id) {
		var m = component.get('c.setComment');
		m.setParams({
			ideaId: id
			, comment: component.get('v.comment')
		});
		m.setCallback(this, function(response) {
			//console.log('getIdea: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.comment', '');
				this.getComments(component, id);
			}
		});
		$A.enqueueAction(m);
	}
	, voteIdea: function(component, ideaId, value) {
		var m = component.get('c.voteIdea');
		//console.log('ID: ' + ideaId + ' -- VAL: ' + value);
		m.setParams({
			ideaId: ideaId
			, value: value
		});
		m.setCallback(this, function(response) {
			//console.log('voteIdea: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				////console.log('voteIdea: ' + JSON.stringify(response.getReturnValue()));
				this.getIdea(component, ideaId);
			} else if (state == 'ERROR') {
				this.showError(component, response.getError());
			} else {
				//console.log('voteIdea errors');
			}
		});
		$A.enqueueAction(m); 
	}
    , showError: function(component, errors) {
    	//console.log('ERRORS: ' + JSON.stringify(errors));
    	if (errors) {
    		if (errors[0] && errors[0].message) {
		    	var t = $A.get("e.force:showToast");
		    	t.setParams({
		    		'title' : 'Error'
		    		, 'message' : errors[0].message
		    		, 'type' : 'error'
		    	});
		    	t.fire();
    		}
    	}
    }
})