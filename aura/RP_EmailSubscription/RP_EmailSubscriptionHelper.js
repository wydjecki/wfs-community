({
	getSubscription: function(component) {
		var m = component.get('c.getCommunicationPreferences');
		m.setCallback(this, function(response) {
			console.log('getSubscription: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				var c = response.getReturnValue();
				console.log('RES: ' + JSON.stringify(c));
				component.set('v.prefs', c);
				var prefs = [];
				for (key in c) {
					prefs.push({'key': key, 'value': c[key]});
				}
				console.log('prefs: ' + prefs);
				component.set('v.prefs', prefs);
			} else {
			}
		});
		$A.enqueueAction(m);
	}
	, setSubscription: function(component) {
		console.log('setSubscription');
		var prefs = component.get('v.prefs');
		var m = component.get('c.setCommunicationPreferences');
		m.setParams({
			prefJson: JSON.stringify(prefs)
		});
		m.setCallback(this, function(response) {
			console.log('getSubscription: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
		    	var t = $A.get("e.force:showToast");
		    	t.setParams({
		    		'title' : 'Subscriptions'
		    		, 'message' : 'Your subscriptions settings have been updated'
		    		, 'type' : 'success'
		    	});
		    	t.fire();

				this.getSubscription(component);
			} else {
			}
		});
		$A.enqueueAction(m);
	}
})