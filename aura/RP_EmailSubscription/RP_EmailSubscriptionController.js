({
	initialize: function(component, event, helper) {
		helper.getSubscription(component);
	}
	, pressSave: function(component, event, helper) {
		helper.setSubscription(component);
	}
})