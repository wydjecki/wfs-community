({
	ready: function(component) {
		console.log('READY');
	}
	, popover: function(component) {
		var p = component.find('pop');
		$A.util.removeClass(p, 'slds-hide');
	}
	, popout: function(component) {
		var p = component.find('pop');
		if (!$A.util.hasClass(p, 'slds-hide')) $A.util.addClass(p, 'slds-hide');
	}

})