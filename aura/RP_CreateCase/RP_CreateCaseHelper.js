({
	getCaseFields : function(component) {
		var action = component.get('c.getNewCase');
		action.setCallback(this, function(response) {
			console.log('GET NEW CASE: ' + response.getReturnValue());
			var state = response.getState();
			if (state === 'SUCCESS') {
				var nc = response.getReturnValue();
				console.log('NC: ' + JSON.stringify(nc));
				//var r = component.get('v.case');
				var r = {'sobjectType' : 'Case', 'Status': 'New', 'Version__c' : ''};
				console.log('C: ' + JSON.stringify(r));

				r.Version__c = nc.record.Version__c;
				console.log('C": ' + JSON.stringify(r));
				component.set('v.case', r);
				component.set('v.newCase', nc);
				this.populatePicklist(component, 'type', nc.picklistMap['Type'], 'Pick a type...');
				this.populatePicklist(component, 'priority', nc.picklistMap['Priority'], 'Pick a priority...');
				this.populatePicklist(component, 'environment', nc.picklistMap['Is_this_a_change_in_Production__c'], 'Pick an environment...');
				this.populatePicklist(component, 'category', nc.picklistMap['Category__c'], 'Pick a category...');
				this.populatePicklist(component, 'severity', nc.picklistMap['Severity__c'], 'Pick a severity...');

				// More fields based displayed on Incident case types
				this.populatePicklist(component, 'systemDown', nc.picklistMap['Has_this_issue_occurred_before__c'], 'Pick a value...');
				this.populatePicklist(component, 'impact', nc.picklistMap['Payroll_impact_in_next_2_days__c'], 'Pick a value...');
				this.populatePicklist(component, 'widespread', nc.picklistMap['Issue_of_Widespread_nature__c'], 'Pick a value...');
				this.populatePicklist(component, 'browsers', nc.picklistMap['Browser_s_affected__c'], '');
				this.populatePicklist(component, 'upgraded', nc.picklistMap['EmpCenter_upgraded_or_patched_last_month__c'], 'Pick a value...');

				//case topic
				this.populateTopicPicklist(component, 'topic', nc.picklistMap['Case_Topic__c']);
			} else if (state === 'ERROR') {
				console.log('ERROR: ' + JSON.stringify(response.getError()));
			}
		});
		$A.enqueueAction(action);
	}
	, populatePicklist: function(component, field, values, placeholder) {
		var options = [];
		if (placeholder != null) options.push({
			class: 'optionClass'
			, label: placeholder
			, value: ''
		});
		for (var i=0; i<values.length; i++)
			options.push({
				class: 'optionClass'
				, label: values[i]
				, value: values[i]
			})
		var e = component.find(field);
		e.set('v.options', options);
		e.set('v.disabled', false);

	}
	, populateDependentPicklist: function(component, field, caseField, parentValue) {
		var action = component.get('c.getCaseDependentPicklist');
		action.setParams({
			fieldName: caseField
			, fieldValue: parentValue
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				var values = response.getReturnValue();
				this.populatePicklist(component, field, values, 'Pick a value...');
			} else if (state === 'ERROR') {
				console.log('ERROR: ' + JSON.stringify(response.getError()));
			}
		});
		$A.enqueueAction(action);

	}

	, populateTopicPicklist: function(component, field, values) {
		var options = [];
		var parentValue = '';
		var e = component.find(field);
		if (values.length == 0) {
			e.set('v.disabled', true);
		} else {
			if (values.length == 1) {
				parentValue = values[0];
				e.set('v.value', parentValue);
			} else {
				options.push({
					class: 'optionClass'
					, label: 'Pick a value...'
					, value: ''
				});
			}
			for (var i=0; i<values.length; i++)
				options.push({
					class: 'optionClass'
					, label: values[i]
					, value: values[i]
				})
			e.set('v.disabled', false);
		}
		e.set('v.options', options);
		console.log('PARENT VALUE: ' + parentValue);
		if (field == 'topic') {
			this.populateTopicDependentPicklist(component, 'serviceArea', 'Service_Area__c', parentValue);
		} else if (field == 'serviceArea') {
			this.populateTopicDependentPicklist(component, 'service', 'Service__c', parentValue);
		} else if (field == 'service') {
			this.populateTopicDependentPicklist(component, 'request', 'Request__c', parentValue);
		}

	}

	, populateTopicDependentPicklist: function(component, field, caseField, parentValue) {
		if (parentValue == null || parentValue == '') {
			this.populateTopicPicklist(component, field, []);
			return;
		}
		var action = component.get('c.getCaseDependentPicklist');
		action.setParams({
			fieldName: caseField
			, fieldValue: parentValue
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				var values = response.getReturnValue();
				this.populateTopicPicklist(component, field, values);
			} else if (state === 'ERROR') {
				console.log('ERROR: ' + JSON.stringify(response.getError()));
			}
		});
		$A.enqueueAction(action);

	}

	, toggleMore: function(component) {
		var more = component.find('more');
		var type = component.get('v.case.Type');
		if (type == 'Incident') {
			$A.util.removeClass(more, 'slds-hide');
		} else {
			if (!$A.util.hasClass(more, 'slds-hide')) $A.util.addClass(more, 'slds-hide');
		}
	}

	, saveCase: function(component) {
		var r = component.get('v.case');
		console.log('CASE: ' + JSON.stringify(r));
		var action = component.get('c.insertCase');
		action.setParams({
			'newCase': r
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				var r = response.getReturnValue();

				var t = $A.get('e.force:showToast');
				t.setParams({
					'title' : 'Success'
					, 'message' : 'Your case has been successfully submitted.'
					, 'key' : 'check'
					, 'type' : 'success'
				});
				t.fire();

		        var a = $A.get('e.force:navigateToURL');
		        a.setParams({
					'url': '/case/' + r.Id
		        });
		        a.fire();
			} else {
				var t = $A.get('e.force:showToast');
				t.setParams({
					'title' : 'Error'
					, 'message' : 'Failed to submit your case.'
					, 'type' : 'error'
				});
				t.fire();
			}
		});
		$A.enqueueAction(action);
	}



})