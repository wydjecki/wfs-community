({
	initialize : function(component, event, helper) {
		helper.getCase(component);
	}

	, pressClose: function(component, event, helper) {
        var p = component.find('prompt');
        $A.util.removeClass(p, 'slds-hide');
	}

	, pressYes: function(component, event, helper) {
		helper.closeCase(component);
		var p = component.find('prompt');
		if (!$A.util.hasClass(p, 'slds-hide')) $A.util.addClass(p, 'slds-hide');
	}

	, pressNo: function(component, event, helper) {
		var p = component.find('prompt');
		if (!$A.util.hasClass(p, 'slds-hide')) $A.util.addClass(p, 'slds-hide');
	}

	, showSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		$A.util.removeClass(s, 'slds-hide');
	}

	, hideSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	}
})