({
    getCase: function(component) {
        var m = component.get('c.getCase');
        m.setParams({
            caseId: component.get('v.recordId')
        });
        m.setCallback(this, function(response) {
            console.log('getCase: ' + JSON.stringify(response));
            var state = response.getState();
            if (state == 'SUCCESS') {
                var c = response.getReturnValue();
                component.set('v.case', c);
                console.log('Case: ' + JSON.stringify(c));
            }
        });
        $A.enqueueAction(m);
    }
    , closeCase: function(component) {
        var c = component.get('v.case');
        var status = c.Status;
        var m = component.get('c.closeCase');
        m.setParams({
            caseId: component.get('v.recordId')
        });
        m.setCallback(this, function(response) {
            console.log('closeCase: ' + JSON.stringify(response));
            var state = response.getState();
            if (state == 'SUCCESS') {
                if (status != 'Resolved') {
                    $A.get('e.force:refreshView').fire();
                    var c = component.get('v.case');
                    console.log('CASE: ' + c.Id + ' -- ' + c.ContactId);
                    window.location = '/customers/s/satsurvey?recordId=' + c.Id + '&cid=' + c.ContactId;
                    
                    /*
                    var a = $A.get('e.force:navigateToURL');
                    a.setParams({
                        'url' : '/satsurvey?recordId=' + c.Id + '&cid=' + c.ContactId
                        , 'isredirect' : true
                    });
                    a.fire();
                    */
                } else {
                    console.log('Status: ' + status);
                    $A.get('e.force:refreshView').fire();
                }
            }
        });
        $A.enqueueAction(m);
    }

 })