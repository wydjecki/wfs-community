({
	initialize: function(component, event, helper) {
	}
	, handleKey: function(component, event, helper){
		if (event.keyCode === 13) {
			helper.gotoSearch(component);
		}
	}
	, handleSearch: function(component, event, helper){
		helper.gotoSearch(component);
	}
})