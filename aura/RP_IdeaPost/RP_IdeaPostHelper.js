({
	setIdea: function(component) {
		var m = component.get('c.setIdea');
        var i = component.get('v.idea');
        //console.log('IDEA: ' + JSON.stringify(i));
		m.setParams({
			newIdea: i
		});
		m.setCallback(this, function(response) {
			//console.log('setIdea: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				//console.log('setIdea Value: ' + JSON.stringify(response.getReturnValue()));
				this.gotoDetail(component, response.getReturnValue());
            } else {
                //console.log(response.getState() + ': ');
            }
		});
		$A.enqueueAction(m); 

	}
    , gotoDetail: function(component, id) {
    	//console.log('gotoDetail: ' + id);
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
        	'url' : '/community-idea?id=' + id
        });
        a.fire();
    }
})