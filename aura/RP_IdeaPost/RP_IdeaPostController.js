({
	initialize: function(component, event, helper) {
        var i = {'sobjectType' : 'Idea'};
        component.set('v.idea', i);
	}
	, handlePost: function(component, event, helper) {
		helper.setIdea(component);
	}
	, showSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		$A.util.removeClass(s, 'slds-hide');
	}

	, hideSpinner: function(component, event, helper) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	}

})