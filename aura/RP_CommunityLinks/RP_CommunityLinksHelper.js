({
	getCommunityLinks: function(component) {
		var m = component.get('c.getComLinks');
		m.setCallback(this, function(response) {
			console.log('getCommunityLinks: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.links', response.getReturnValue());
			console.log('Links: ' + JSON.stringify(response.getReturnValue()));
			}
		});
		$A.enqueueAction(m);

	}
})