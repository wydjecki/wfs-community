({
	initialize: function (component, event, helper) {
		helper.getCommunityLinks(component);
	},

	goToLink: function(component, event, helper) {
		'use strict';
		var url = event.currentTarget.getAttribute("data-URL");
		console.log(url);
			window.open('https://' + url);
		}
})