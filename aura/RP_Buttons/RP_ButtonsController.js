({
    initialize: function(component, event, helper) {
    }

	, gotoPostIdea: function(component, event, helper) {
		console.log('gotoPostIdea');
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
        	'url' : '/community-idea-post'
        });
        a.fire();
	}

  , gotoIdeas: function(component, event, helper) {
		console.log('gotoIdeas');
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
        	'url' : '/community-ideas'
        });
        a.fire();
	}

	, gotoCaseReport: function(component, event, helper) {
		console.log('gotoCaseReport');
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
        	'url' : '/report/00On0000000K2Ya'
        });
        a.fire();
    }
    , gotoCaseCreation: function(component, event, helper) {
        console.log('gotoCaseCreation');
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
            'url' : '/casecreation'
        });
        a.fire();
    }
    , gotoSubscription: function(component, event, helper) {
        console.log('gotoSubscription');
        var a = $A.get('e.force:navigateToURL');
        a.setParams({
            'url' : '/subscriptions'
        });
        a.fire();
    }
})