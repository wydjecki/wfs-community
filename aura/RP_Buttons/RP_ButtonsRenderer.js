({
	afterRender: function(component, helper) {
        var name = component.get('v.buttonName');
        console.log('BTN NAME: ' + name);
        var btn = component.find(name);
        console.log('BTN: ' + JSON.stringify(btn));
        $A.util.removeClass(btn, 'slds-hide');
	}
})