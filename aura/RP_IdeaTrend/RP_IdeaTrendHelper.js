({
	getIdeas: function(component) {
		var m = component.get('c.getTrendingIdeas');
		m.setCallback(this, function(response) {
			//console.log('getTrendingIdeas: ' + JSON.stringify(response));
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.ideas', response.getReturnValue());
				//console.log('getTrendingIdeas: ' + JSON.stringify(response.getReturnValue()));
			} 
		});
		$A.enqueueAction(m); 
	}
})