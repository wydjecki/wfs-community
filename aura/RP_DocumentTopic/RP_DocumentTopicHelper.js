({
	getTopics : function(component) {
		this.showSpinner(component);
	    var m = component.get('c.getTopics');
	    m.setCallback(this, function(response) {
	    	//console.log('RESPONSE: ' + response.getReturnValue());
	    	var state = response.getState();
			this.hideSpinner(component);
	    	if (state == 'SUCCESS') {
	    		//console.log('STATE SUCCESS: ' + response.getReturnValue());
	    		var jsonString = response.getReturnValue();
	    		//console.log('RES: ' + JSON.stringify(jsonString));
	    		component.set('v.topic', response.getReturnValue());
			} else {
				//console.log('ERROR: ' + JSON.stringify(response.getError()));
			}
		});
	    $A.enqueueAction(m);
	}
	, showSpinner: function(component) {
    	var s = component.find('spinner');
    	$A.util.removeClass(s, 'slds-hide');
  	}

	, hideSpinner: function(component) {
		var s = component.find('spinner');
		if (!$A.util.hasClass(s, 'slds-hide')) $A.util.addClass(s, 'slds-hide');
	}

})