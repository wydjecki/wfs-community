({
	getAnnouncements : function(component) {
		var currentUser = component.get("v.userType")
      var action = component.get('c.getAnnouncements');
				action.setParams({
					currentUser : currentUser
				});
      action.setCallback(this, function(response) {
            var state = response.getState();
            // console.log('RESPONSE: ' + response);
            if (state == 'SUCCESS') {
                component.set('v.announcements', response.getReturnValue());
            } else if (state == 'ERROR') {
                // console.log('ERROR: ' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

		getCurrentUser: function(component) {
			var action = component.get('c.currentUserType');
			action.setCallback(this, function(response) {
						var userType = component.get("v.userType");
						var state = response.getState();
						// console.log('RESPONSE: ' + response);
						if (state == 'SUCCESS') {
								component.set('v.userType', response.getReturnValue());
	              this.getAnnouncements(component);
						} else if (state == 'ERROR') {
								// console.log('ERROR: ' + JSON.stringify(response.getError()));
						}
				});
				$A.enqueueAction(action);
		},

})