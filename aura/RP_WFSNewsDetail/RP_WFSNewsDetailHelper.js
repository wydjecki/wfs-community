({
	getNews : function(component) {
		console.log('helper');
			var recordId = component.get('v.recordId');
			var action = component.get('c.showNewsDetail');
			action.setParams({
							recordId : recordId
						});
						console.log('RecordId: ' + recordId);
			action.setCallback(this, function(response) {
						var state = response.getState();
						console.log('RESPONSE: ' + response);
						if (state == 'SUCCESS') {
								component.set('v.news', response.getReturnValue());
						} else if (state == 'ERROR') {
								console.log('ERROR: ' + JSON.stringify(response.getError()));
						}
				});
				$A.enqueueAction(action);
		},
})