@isTest
private class RP_AnnouncementControllerTest {

    @isTest static void test_currentUserType() {
        User partner = [Select Id From User Where LastName = 'Test Partner' limit 1];
        User customer = [Select Id From User Where LastName = 'Test Customer' limit 1];
        String partnerUserType, customerUserType, blankUserType;

        Test.startTest();
            System.runAs(partner) {
            partnerUserType = RP_AnnouncementController.currentUserType();
            }
            System.runAs(customer) {
            customerUserType = RP_AnnouncementController.currentUserType();
            }
            blankUserType = RP_AnnouncementController.currentUserType();
        Test.stopTest();

        System.assertEquals('Partner', partnerUserType);
        System.assertEquals('Customer', customerUserType);
        System.assertEquals('', blankUserType);
    }

    @isTest static void test_getAnnouncements() {
        RP_AnnouncementController.getAnnouncements('partner');
        RP_AnnouncementController.getAnnouncements('customer');
        RP_AnnouncementController.getAnnouncements('partner');
    }

    @testSetup
    static void setup() {
        Account acc = new Account(name='Test Acc', EC_Production_Version__c = '1234');
        insert acc;
        System.assertEquals('Test Acc', acc.Name);

        Contact con = new Contact(lastname='Test Partner', AccountId=acc.Id, Email='TestEmail@gmail.com');
        insert con;
        System.assertEquals('Test Partner', con.LastName);
        Contact con1 = new Contact(lastname='Test Customer', AccountId=acc.Id, Email='TestEmail@gmail.com');
        insert con1;
        System.assertEquals('Test Customer', con1.LastName);

        Id partner = [Select Id, name FROM Profile Where Name = 'Partner Community Plus User'].Id;
		Id customer = [Select Id, name FROM Profile Where Name = 'Customer Community Plus Login User - Custom'].Id;
		Id superCustomer = [Select Id, name FROM Profile Where Name = 'Customer Community Plus Login Admin User - Custom'].Id;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

        User us1 = new User(
        Alias = 'test1234'
        , Email='testuser@testorg1.com'
        , EmailEncodingKey='UTF-8'
        , LastName='Testing1'
        , LanguageLocaleKey='en_US'
        , LocaleSidKey='en_US'
        , ProfileId = p.Id
        , TimeZoneSidKey='America/Los_Angeles'
        , UserName='guestusermanager@testorg.com'
        );
        insert us1;
        System.assertEquals('Testing1', us1.LastName);

        User us = new User(
        Alias = 'test123'
        , Email='testuser@testorg.com'
        , EmailEncodingKey='UTF-8'
        , LastName='Test Partner'
        , LanguageLocaleKey='en_US'
        , LocaleSidKey='en_US'
        , ProfileId = partner
        , ContactId = con.Id
        , TimeZoneSidKey='America/Los_Angeles'
        , UserName='guestuser@testorg.com'
        , ManagerId = us1.Id
        );
        insert us;
        System.assertEquals('Test Partner', us.LastName);

        User us2 = new User(
        Alias = 'test123'
        , Email='testuser@testorg.com'
        , EmailEncodingKey='UTF-8'
        , LastName='Test Customer'
        , LanguageLocaleKey='en_US'
        , LocaleSidKey='en_US'
        , ContactId = con1.Id
        , ProfileId = customer
        , TimeZoneSidKey='America/Los_Angeles'
        , UserName='guestusercustomer@testorg.com'
        , ManagerId = us1.Id
        );
        insert us2;
        System.assertEquals('Test Customer', us2.LastName);
        }
}