/**************************************************
* Written with passion by awidjaja@rightpoint.com
* 2017/04/04 - v1.0
***************************************************/

public class ProcessBuilderSetting {
	@InvocableMethod
	public static void log() {
		System.debug('Process Builder is turned off for ' + UserInfo.getUserName());
	}
}