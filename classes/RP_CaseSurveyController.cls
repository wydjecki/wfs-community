public without sharing class RP_CaseSurveyController {
	@AuraEnabled
	public static Case_Survey__c getSurvey(String caseId, String contactId) {
		System.debug('GET SURVEY: ' + caseId + ' : ' + contactId);
		return [
			SELECT Id, Case_Subject__c, Status__c
			, Score__c, Comment__c
			FROM Case_Survey__c
			WHERE Case__c = :caseId
			AND Contact__c = :contactId
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
	}

	@AuraEnabled
	public static void saveSurvey(Case_Survey__c survey) {
		try {
			survey.Taken_Date__c = System.now();
			survey.Status__c = 'Completed';
			update survey;

		} catch (Exception ex) {
			System.debug('SAVE SURVEY [' + survey + ']: ' + ex.getMessage());
			throw new AuraHandledException('Failed to save survey.');
		}
	}

}