public class RP_IdeaController {
	@testVisible private static String ZONE_NAME = 'Customer Community';
    @testVisible private static String STATUS_CLOSED = 'Delivered';
	private static final Integer TREND_LIMIT = 10;
	


	@AuraEnabled 
	public static IdeaList getIdeaList(String keyword, String statusFilter, Integer pageSize, Integer startRow) {
		IdeaList result = new IdeaList();
		try {
			String whereFilter = '';
			if (statusFilter == 'Delivered') {
				whereFilter += ' AND Status = \'' + STATUS_CLOSED + '\'';
			} else if (statusFilter == 'Undelivered') {
				whereFilter += ' AND Status != \'' + STATUS_CLOSED + '\'';
			}
			String orderLimit = ' ORDER BY CreatedDate DESC LIMIT ' + pageSize + ' OFFSET ' + startRow;


			List<Idea> ideas = null;
			List<Idea> allIdeas = null;
			if (String.isBlank(keyword)) {
				String soql = 'SELECT Id'
					+ ' FROM Idea WHERE Community.Name = \'' + ZONE_NAME + '\''
					+ whereFilter
					;
				allIdeas = Database.query(soql);
				soql += orderLimit;
				ideas = Database.query(soql);
			} else {
				String sosl = 'FIND \'' + keyword + '\''
					+ ' IN ALL FIELDS'
					+ ' RETURNING Idea(Id WHERE Community.Name = \'' + ZONE_NAME + '\''
					+ whereFilter
					+ ')'
					;
				List<List<sObject>> results = search.query(sosl);
				allIdeas = (List<Idea>) results[0];
				sosl = sosl.replace(')', orderLimit + ')');
				results = search.query(sosl);
				ideas = (List<Idea>) results[0];
			}
			result.total = allIdeas.size();
			if (!ideas.isEmpty()) {
				result.ideas = [
					SELECT Id, Title, Status
					, CreatedDate, CreatedById, Body, Categories
					, CreatorSmallPhotoUrl, CreatorName
					, NumComments, VoteTotal
					, (SELECT Id, Type FROM Votes WHERE CreatedById = :UserInfo.getUserId())
					FROM Idea
					WHERE Id IN :ideas
				];
			}
		} catch (Exception ex) {
			System.debug('getIdeaList: ' + ex.getMessage());
			throw new AuraHandledException('Failed to read ideas.');
		}
		return result;
	}

	@AuraEnabled 
	public static Integer getIdeasCount(String keyword, String statusFilter) {
		Integer count = 0;
		try {
			String whereFilter = '';
			if (statusFilter == 'Delivered') {
				whereFilter += ' AND Status = \'' + STATUS_CLOSED + '\'';
			} else if (statusFilter == 'Undelivered') {
				whereFilter += ' AND Status != \'' + STATUS_CLOSED + '\'';
			}

			List<Idea> ideas = new List<Idea>();
			if (String.isBlank(keyword)) {
				String soql = 'SELECT Id'
					+ ' FROM Idea WHERE Community.Name = \'' + ZONE_NAME + '\''
					+ whereFilter
					;
				ideas = Database.query(soql);
			} else {
				String sosl = 'FIND \'' + keyword + '\''
					+ ' IN ALL FIELDS'
					+ ' RETURNING Idea(Id WHERE Community.Name = \'' + ZONE_NAME + '\''
					+ whereFilter
					+ ')'
					;

				List<List<sObject>> query = search.query(sosl);
				ideas = (List<Idea>) query[0];
			}
			count = ideas.size();
		} catch (Exception ex) {
			System.debug('getIdeas: ' + ex.getMessage());
			throw new AuraHandledException('Failed to read ideas.');
		}
		return count;
	}

	@AuraEnabled 
	public static List<Idea> getTrendingIdeas() {
		try {
			return [
				SELECT Id, Title, Status
				, CreatedDate, Body, Categories
				, CreatorSmallPhotoUrl, CreatorName
				, NumComments, VoteTotal, LastViewedDate
				FROM Idea
				WHERE Community.Name = :ZONE_NAME
				AND Status != :STATUS_CLOSED
				ORDER BY VoteScore DESC
				LIMIT :TREND_LIMIT
			];
		} catch (Exception ex) {
			System.debug('getTrendingIdeas: ' + ex.getMessage());
			throw new AuraHandledException('Failed to read ideas.');
		}
	}

	@AuraEnabled
	public static Idea getIdea(String ideaId) {
		return [
			SELECT Id, Title, Status
			, CreatedDate, Body, Categories
			, CreatorSmallPhotoUrl, CreatorName
			, NumComments, VoteTotal
			, (SELECT Id, Type FROM Votes WHERE CreatedById = :UserInfo.getUserId())
			FROM Idea
			WHERE Id = :ideaId
		];
	}
    
    @AuraEnabled
    public static String tryIdea(Idea newIdea) {
        System.debug('TRY IDEA: ' + newIdea);
        return 'SUCCESS!';
    }

	@AuraEnabled
	public static String setIdea(Idea newIdea) {
        System.debug('NEW IDEA: ' + newIdea);
		String newId = '';
		try {
            
			User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
			if (u.ContactId != null) {
				newIdea.Account__c = [SELECT AccountId FROM Contact WHERE Id = :u.ContactId].AccountId;
			} 
			newIdea.CommunityId = [SELECT Id  FROM Community WHERE Name = :ZONE_NAME].Id;
			insert newIdea;
			newId = newIdea.Id;
		} catch(Exception ex) {
			System.debug('setIdea [' + newIdea + ']: ' + ex.getMessage());
			throw new AuraHandledException('Failed to submit new idea.');	
		}
		return newId;
	}

	@AuraEnabled
	public static List<IdeaComment> getComments(String ideaId) {
		return [
			SELECT Id, CreatedDate, CommentBody
			, CreatorSmallPhotoUrl, CreatorName
			FROM IdeaComment
			WHERE IdeaId = :ideaId
			ORDER BY CreatedDate DESC
		];	
	}

	@AuraEnabled
	public static void setComment(String ideaId, String comment) {
		if (String.isBlank(comment)) return;
		try {
			insert new IdeaComment(
				IdeaId = ideaId
				, CommentBody = comment
			);
		} catch (Exception ex) {
			System.debug('setComment [' + ideaId + ']: ' + ex.getMessage());
			throw new AuraHandledException('Failed to leave a comment.');
		}
	}

	@AuraEnabled
	public static void voteIdea(String ideaId, String value) {
		try {
			insert new Vote(
				ParentId = ideaId
				, Type = value
			);
		} catch (Exception ex) {
			System.debug('voteIdea [' + ideaId + ']: ' + ex.getMessage());
			if (ex.getMessage().contains('DUPLICATE_VALUE')) {
				throw new AuraHandledException('You have voted for this idea.');
			} else {
				throw new AuraHandledException('Failed to vote for idea.');
			}
		}
	}
	
    @AuraEnabled
	public static IdeaList getIdeaListOLD(String keyword, String statusFilter, Integer pageSize, Integer startRow) {
		IdeaList result = new IdeaList();
		try {
			String whereFilter = '';
			if (statusFilter == 'Delivered') {
				whereFilter += ' AND Status = \'' + STATUS_CLOSED + '\'';
			} else if (statusFilter == 'Undelivered') {
				whereFilter += ' AND Status != \'' + STATUS_CLOSED + '\'';
			}
			String orderLimit = ' ORDER BY CreatedDate DESC LIMIT ' + pageSize + ' OFFSET ' + startRow;
	
			//get total
			result.total = getIdeasCount(keyword, statusFilter);
	
			if (String.isBlank(keyword)) {
				String soql = 'SELECT Id, Title, Status'
					+ ', CreatedDate, Body, Categories'
					+ ', CreatorSmallPhotoUrl, CreatorName'
					+ ', NumComments, VoteTotal'
					+ ', (SELECT Id, Type FROM Votes WHERE CreatedById = \'' + UserInfo.getUserId() + '\')'
					+ ' FROM Idea WHERE Community.Name = \'' + ZONE_NAME + '\''
					+ whereFilter
					+ orderLimit
					;
				result.ideas = Database.query(soql);
	
			} else {
				String sosl = 'FIND \'' + keyword + '\''
					+ ' IN ALL FIELDS'
					+ ' RETURNING Idea(Id WHERE Community.Name = \'' + ZONE_NAME + '\''
					+ whereFilter
					+ orderLimit
					+ ')';
	
				List<List<sObject>> query = search.query(sosl);
				if (!query.isEmpty()) {
					result.ideas = [
						SELECT Id, Title, Status
						, CreatedDate, Body, Categories
						, CreatorSmallPhotoUrl, CreatorName
						, NumComments, VoteTotal
						, (SELECT Id, Type FROM Votes WHERE CreatedById = :UserInfo.getUserId())
						FROM Idea
						WHERE Id IN :query[0]
					];
				}
				//result.ideas = (List<Idea>) query[0];
	
			}
	
		} catch (Exception ex) {
			System.debug('getIdeas: ' + ex.getMessage());
			throw new AuraHandledException('Failed to read ideas.');
		}
		return result;
	}
	
	@AuraEnabled
	public static List<Idea> getIdeasOLD(String keyword, String statusFilter, Integer pageSize, Integer startRow) {
		List<Idea> ideas = null;
		try {
			String whereFilter = '';
			if (statusFilter == 'Delivered') {
				whereFilter += ' WHERE Status = \'' + STATUS_CLOSED + '\'';
			} else if (statusFilter == 'Undelivered') {
				whereFilter += ' WHERE Status != \'' + STATUS_CLOSED + '\'';
			}
			String orderLimit = ' ORDER BY CreatedDate DESC LIMIT ' + pageSize + ' OFFSET ' + startRow;
	
			if (String.isBlank(keyword)) {
				String soql = 'SELECT Id, Title, Status'
					+ ', CreatedDate, Body, Categories'
					+ ', CreatorSmallPhotoUrl, CreatorName'
					+ ', NumComments, VoteTotal'
					+ ' FROM Idea'
					+ whereFilter
					+ orderLimit
					;
				ideas = Database.query(soql);
	
			} else {
				String sosl = 'FIND \'' + keyword + '\''
					+ ' IN ALL FIELDS'
					+ ' RETURNING Idea('
					+ ' Id, Title, Status'
					+ ', CreatedDate, Body, Categories'
					+ ', CreatorSmallPhotoUrl, CreatorName'
					+ ', NumComments, VoteTotal'
					+ whereFilter
					+ orderLimit
					+ ')';
	
				List<List<sObject>> query = search.query(sosl);
				ideas = (List<Idea>) query[0];
			}
	
		} catch (Exception ex) {
			System.debug('getIdeas: ' + ex.getMessage());
			throw new AuraHandledException('Failed to read ideas.');
		}
		return ideas;
	}

	public class IdeaList {
		@AuraEnabled public Integer total = 0; //total of records without pagination
		@AuraEnabled public List<Idea> ideas = new List<Idea>();
	}
}