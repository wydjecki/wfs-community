@isTest
global class UtilityServiceLoginMock implements WebServiceMock {
	global void doInvoke(
			Object stub
			, Object request
			, Map<String, Object> response
			, String endpoint
			, String soapAction
			, String requestName
			, String responseNS
			, String responseName
			, String responseType
		) {

		UtilityService.loginResponse_element res = new UtilityService.loginResponse_element();
		res.result = new UtilityService.LoginResult();
		response.put('response_x', res);

	}

}