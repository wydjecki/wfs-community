public virtual class RP_BaseController { 

	public static User getCurrentUser() {
		return [
			SELECT Id, FirstName, LastName
			, Email, ContactId
			FROM User
			WHERE Id = :UserInfo.getUserId()
		];
	}

}