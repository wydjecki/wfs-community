public without sharing class RP_DocumentService {
    private static final String SEARCH_PROPERTIES = 'Title,Path,FileType,FileExtension,HitHighlightedProperties,LastModifiedTime,RefinableCommunityAudience,RefinablePartnerRole,RefinablePartnerType,RefinableCommunityProduct,RefinablePartnerEcosystem,RefinableCommunityPortalCategory,RefinableWhoCanView,AllCommunityProduct,AllPartnerEcosystem,RefinableTeam,AllCommunityPortalCategory,ServerRedirectedPreviewUrl,ServerRedirectedEmbedUrl,DefaultEncodingURL,IsContainer,IsDocument';

    private static Community_Setting__c setting;

    static {
        setting = Community_Setting__c.getInstance(UserInfo.getUserId());
    }


    public static String getBearerToken(){
        HttpRequest req = new HttpRequest();
        // use these named credentials to talk to the endpoint that gives you the bearer token you need to call into SP
        req.setEndpoint('callout:WFS_SharePoint_Token/token');
        // API endpoint won't let you in without sending this header
        req.setHeader('X-ClientName', 'WFS-Salesforce');
        req.setMethod('POST');
        String JsonString = JSON.serialize('');
        req.setBody(JsonString);

        Http h = new Http();   
        HttpResponse res = h.send(req);
        
        String bearerToken = res.getBody().replace('"', '');
        System.debug('TOKEN: ' + bearerToken);
        return bearerToken;
    }
    
    public static String getTopics() {
        HttpRequest req = new HttpRequest();
        //req.setEndpoint(TOPICS_API_URL);
        req.setEndpoint(setting.Document_Topic_URL__c);
        req.setHeader('X-ClientName', 'WFS-Salesforce');
        req.setHeader('Content-Type','application/json');
        req.setMethod('GET');
     
        Http h = new Http();
        HttpResponse res = h.send(req);
        String status = res.toString();
        System.debug('STATUS' + status);
     
        String topics = res.getBody();
        //System.debug('Topics Response ' + topics);
        System.debug('Topics Response JSON ' + JSON.serialize(topics));
        return topics;
    }    

    public static String getProducts() {
        HttpRequest req = new HttpRequest();
        //req.setEndpoint(TOPICS_API_URL);
        req.setEndpoint(setting.Document_Product_URL__c);
        req.setHeader('X-ClientName', 'WFS-Salesforce');
        req.setHeader('Content-Type','application/json');
        req.setMethod('GET');
     
        Http h = new Http();
        HttpResponse res = h.send(req);
        String status = res.toString();
        System.debug('STATUS' + status);
     
        String topics = res.getBody();
        //System.debug('Topics Response ' + topics);
        System.debug('Products Response JSON ' + JSON.serialize(topics));
        return topics;
    }    
    
    public static String search(String topicId, String keyword, String access, String filters, Integer startRow, Integer rowLimit){
        System.debug('keyword: ' + keyword + ' -- START: ' + startRow + ' -- LIMIT: ' + rowlimit);
        
        HttpRequest req = new HttpRequest();
        String endpoint = buildSearchURL(keyword, topicId, access, filters, startrow, rowlimit);
        System.debug('SEARCH ENDPOINT: ' + endpoint);
        req.setEndpoint(endpoint);
        req.setHeader('Authorization', getBearerToken());
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept', 'application/json;odata=verbose');
        req.setMethod('GET');

        Http h = new Http();
        HttpResponse res = h.send(req);
        String result = res.getBody();
        System.debug('RESULT: ' + result);
        return result;
    }

    public static String getFile(String path) {
        String result = '';

        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:WFS_SharePoint_Token');
        req.setHeader('X-ClientName', 'WFS-Salesforce');
        req.setHeader('Content-Type', 'application/json');
        //req.setHeader('Cookie', getToken());
        req.setMethod('POST');
        req.setBody('{"filePath":"' + path + '"}');

        Http h = new Http();
        HttpResponse res = h.send(req);
        result = res.getBody();
        //Blob data = res.getBodyAsBlob();
        //result = EncodingUtil.base64Encode(data);
        System.debug('GET FILE RESULT: ' + result);
        return result;
    }

    public static String getLink(String path){
        String result = '';

        // this wont work without External Sharing turned on on the site collection and in the organiztion
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:WFS_SharePoint_Token');
        req.setHeader('X-ClientName', 'WFS-Salesforce');
        req.setHeader('Content-Type','application/json');
		req.setMethod('POST');
        req.setBody('{"filePath":"' + path + '"}');

        Http h = new Http();
        HttpResponse res = h.send(req);
        result = res.getBody();
        System.debug('GET LINK RESULT: ' + result);
        return result;
    }


    private static String buildSearchURL(String keyword, String category, String constraints, String filters, Integer startRow, Integer pageSize ) {
        String url = setting.Document_Base_Search_URL__c;
        System.debug('BASE URL: ' + url);

        RP_DocumentSearchRequest req = new RP_DocumentSearchRequest();


        String contentType = 'ContentTypeId:' + setting.Document_Content_Type__c;

        String queryText = '';
        if (String.isNotBlank(keyword)) queryText = keyword;
        if (String.isNotBlank(category)) queryText += ((queryText == '') ? '' : ' AND ') + 'AllCommunityPortalCategory:' + category;

        req.QueryText = queryText;
        req.EnableStemming = true;
        req.EnablePhonetic = false;
        req.EnableNicknames = false;
        req.TrimDuplicates = false;
        req.EnableFql = false;
        req.EnableQueryRules = false;
        req.ProcessBestBets = false;
        req.ByPassResultTypes = false;
        req.ProcessPersonalFavorites = false;
        req.GenerateBlockRankLog = false;
        req.IncludeRankDetail = false;
        req.StartRow = startRow;
        req.RowLimit = pageSize;
        req.RowsPerPage = pageSize;
        req.SelectProperties = SEARCH_PROPERTIES;
        req.GraphQuery = '';
        req.GraphRankingModel = '';
        req.Refiners = setting.Document_Search_Refiners__c;
        req.RefinementFilters = filters;
        req.HitHighlightedProperties = '';
        req.RankingModelId = '';
        req.SortList = '';
        req.Culture = '';
        req.SourceId = setting.Document_Data_Set_GUID__c;
        req.HiddenConstraints = (String.isNotBlank(constraints)? constraints + ' AND ' + contentType : contentType) + ' AND IsDocument:true AND IsContainer:false';
        req.ResultsUrl = '';
        req.QueryTag = '';
        req.CollapseSpecification = '';
        req.QueryTemplate = '';
        req.TrimDuplicatesIncludeId = null;
        req.ClientType = '';
        req.PersonalizationData = '';
        url += req.generateHttpGetUriPart();
        //System.debug('URL: ' + req.generateHttpGetUriPart());
        /* WORKING REFINEMENTFILTER FORMAT:

                'and(RefinableCommunityPortalCategory:and(ǂǂ537570706f7274,ǂǂ53616c657320616e64204d61726b6574696e67)'
                + ',RefinableCommunityProduct:ǂǂ5374616666506c616e6e6572)'
        */
        return url;
    }

}