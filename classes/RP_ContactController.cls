public without sharing class RP_ContactController {

	@AuraEnabled
	public static Map<String, Boolean> getCommunicationPreferences() {
		Map<String, Boolean> prefMap = new Map<String, Boolean>();
		try {

			List<String> options = RP_Utility.getPicklistValues('Contact', 'Customer_Communication_Preference__c');
			System.debug('PREF OPTIONS: ' + options);
			for (String option : options) {
				prefMap.put(option, false);
			}

			User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
			Contact c = [
				SELECT Id, Customer_Communication_Preference__c
				FROM Contact
				WHERE Id = :u.ContactId
			];
			if (String.isNotBlank(c.Customer_Communication_Preference__c)) {
				List<String> prefs = c.Customer_Communication_Preference__c.split(';');
				for (String pref : prefs) {
					if (prefMap.containsKey(pref)) prefMap.put(pref, true);
				}
			}

		} catch (Exception ex) {
			System.debug('getCommunicationPreferences: ' + ex.getMessage());
			throw new AuraHandledException('Failed to read data.');
		}
		return prefMap;
	}

	@AuraEnabled
	public static void setCommunicationPreferences(String prefJson) {
		System.debug('PREF JSON: ' + prefJson);
		List<MapData> data = (List<MapData>) JSON.deserialize(prefJson, List<MapData>.class);
		System.debug('PREF DATA: ' + data);
		try {

			List<String> prefs = new List<String>();
			String comm = '';
			for (MapData d : data) {
				if (d.value) prefs.add(d.key);
			}
			System.debug('PREFS: ' + prefs);
			comm = String.join(prefs, ';');

			User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
			update new Contact(
				Id = u.ContactId
				, Customer_Communication_Preference__c = comm
			);
		} catch (Exception ex) {
			//System.debug('setCommunicationPreferences [' + prefMap + ']: ' + ex.getMessage());
			throw new AuraHandledException('Failed to save preferences.');
		}
	}

	private class MapData {
		String key;
		Boolean value;
	}
}