@isTest
global class UtilityServiceReadMetadataMock implements WebServiceMock {
	global void doInvoke(
			Object stub
			, Object request
			, Map<String, Object> response
			, String endpoint
			, String soapAction
			, String requestName
			, String responseNS
			, String responseName
			, String responseType
		) {

		UtilityService.readRecordTypeResponse_element res = new UtilityService.readRecordTypeResponse_element();
		res.result = new UtilityService.ReadRecordTypeResult();
		res.result.records = new List<UtilityService.RecordType>();
		response.put('response_x', res);

	}

	
}