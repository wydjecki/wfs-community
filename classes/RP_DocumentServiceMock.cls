global class RP_DocumentServiceMock {
	global class HttpCallout implements HttpCalloutMock {
		global HttpResponse respond(HttpRequest req) {
			HttpResponse res = new HttpResponse();
			res.setBody('Response from the HttpCalloutMock');
			res.setStatusCode(200);
			return res;
		}
	}

}