public class RP_NewsController {

	@AuraEnabled
	public static News__c[] getWFSNews(String filter, String sortField, String sortDirection, String currentUser) {
		if (String.isBlank(filter)) filter = 'All';
		if (String.isBlank(sortField)) sortField = 'CreatedDate';
		if (String.isBlank(sortDirection)) sortDirection = 'DESC';
		if (String.isBlank(currentUser)) currentUser = '';
		String soql  =
		'SELECT Id'
			+ ' , Title__c, OwnerId, Details__c'
			+ ' , Article_Image__c, Excerpt__c, RecordType.DeveloperName'
			+ ' , Content_Type__c, External_URL__c'
			+ ' , Partner_Only__c, Customer_Only__c'
			+ ' FROM News__c'
		;

		String filterByContent = ' WHERE Content_Type__c = :filter ';
		if(filter != 'All')
			soql += filterByContent;
			System.debug(soql);

		String andPartner = ' AND Customer_Only__c != true ';
		String wherePartner = ' WHERE Customer_Only__c != true ';
		if (currentUser == 'Partner')
			soql = (filter != 'All') ? (soql += andPartner) : (soql += wherePartner);

		String andCustomer = ' AND Partner_Only__c != true ';
		String whereCustomer = ' WHERE Partner_Only__c != true ';
		if (currentUser == 'Customer')
			soql = (filter != 'All') ? (soql += andCustomer) : (soql += whereCustomer);

		System.debug('Final Soql' + soql);
		return Database.query(soql);
	}

	@AuraEnabled
	public static News__c[] getNewsForHomePage(String currentUser) {
		String soql  =
		'SELECT Id'
			+ ' , Title__c, OwnerId, Details__c'
			+ ' , Article_Image__c, Excerpt__c, RecordType.DeveloperName'
			+ ' , Content_Type__c, External_URL__c'
			+ ' , Partner_Only__c, Customer_Only__c'
			+ ' FROM News__c'
		;
        DateTime systemNow = System.now();
		String andTimeAndPriority =
        ' AND Spotlight__c = true'
			+ '	AND Start_Time__c <= :systemNow'
            + ' AND End_Time__c >= :systemNow'
            + ' ORDER BY CreatedDate DESC'
			+ ' LIMIT 2'
        ;
		String wherePartner = ' WHERE Customer_Only__c != true';
			soql = ( currentUser == 'Partner' ) ? ( soql += wherePartner ) : soql;

		String whereCustomer = ' WHERE Partner_Only__c != true';
			soql = ( currentUser == 'Customer' ) ? ( soql += whereCustomer ) : soql;

        String whereTimeAndPriority = andTimeAndPriority.replaceFirst( ' AND', ' WHERE' );
            soql = String.isBlank(currentUser) ? ( soql += whereTimeAndPriority ) : ( soql += andTimeAndPriority );

        System.debug('SOQL : ' + soql);
		return Database.query(soql);
	}

	@AuraEnabled
	public static News__c showNewsDetail(String recordId) {
        return [
            	SELECT Title__c, Excerpt__c, Article_Image__c
				, External_URL__c, RecordType.DeveloperName
				, CreatedDate__c, CreatedById, CreatedBy.Full_Name__c
				, Content_Type__c, Details__c
            	FROM News__c
				WHERE Id =:recordId
        	];
    }

	@AuraEnabled
	public static String currentUserType() {
		String userType;
		Id partner = [Select Id, name FROM Profile Where Name = 'Partner Community Plus User'].Id;
		Id customer = [Select Id, name FROM Profile Where Name = 'Customer Community Plus Login User - Custom'].Id;
		Id superCustomer = [Select Id, name FROM Profile Where Name = 'Customer Community Plus Login Admin User - Custom'].Id;

		if(UserInfo.getProfileId() == partner) {
			userType = 'Partner';
		} else if(UserInfo.getProfileId() == customer || UserInfo.getProfileId() == superCustomer) {
			userType = 'Customer';
		} else{
			userType = '';
		}
		return userType;
	}

	@AuraEnabled
	public static List<String> getPicklistValues(String objectName, String fieldName) {
		return RP_Utility.getPicklistValues(objectName, fieldName);
	}
}