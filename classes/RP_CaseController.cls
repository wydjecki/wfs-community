public class RP_CaseController {

	@AuraEnabled
	public static List<Case> getMyOpenCases(String sortField, String sortDirection) {
		if (String.isBlank(sortField)) sortField = 'CreatedDate';
		if (String.isBlank(sortDirection)) sortDirection = 'DESC';
		User currentUser = RP_BaseController.getCurrentUser();
		System.debug('CURRENT USER: ' + currentUser);
		String soql =
		'SELECT Id, Subject, Status, Case_Status__c, CreatedDate'
		+ ' FROM Case'
		+ ' WHERE ContactId = \'' + currentUser.ContactId + '\''
		+ ' AND Status != \'Closed\''
		+ ' ORDER BY ' + sortField + ' ' + sortDirection
		+ ' LIMIT 10'
		;
		return Database.query(soql);
	}

	@AuraEnabled
	public static void closeCase (String caseId) {
		try {
			update new Case(
			Id = caseId
			, Status = 'Closed'
			);

		} catch (Exception ex) {
			throw new AuraHandledException('Failed to close the case.');
		}
	}

	@AuraEnabled
	public static boolean caseStillOpen(String recordId) {
		Case c = [SELECT Id, Status FROM Case WHERE Id =:recordId];
		Boolean isStillOpen = false;
		if(c.Status != 'Closed') {
			isStillOpen = true;
		}
		return isStillOpen;
	}

	@AuraEnabled
	public static Case getCase(String caseId) {
		Id communityId = [
		SELECT Id FROM RecordType
		WHERE sObjectType='Case'
		AND DeveloperName = 'Community'
		].Id;
		if (String.isBlank(caseId)) {
			return new Case(RecordTypeId = communityId);
		} else {
			return [
			SELECT Id, Type, Service__c, Service_Area__c, Request__c
			, Category__c, Customer_Category__c, Version__c, Priority, Which_Environment__c
			, Status, Subject, Description, ContactId, AccountId
			FROM Case
			WHERE
			//RecordTypeID =: communityId
			Id = :caseId
			];
		}
	}

	@AuraEnabled
	public static NewCase getNewCase() {
		Contact cnt = null;
		try {
			User u = [
			SELECT ContactId
			FROM User
			WHERE Id = :UserInfo.getUserId()
			];
			if (u.ContactId != null) {
				cnt = [
				SELECT AccountId, Account.EC_Production_Version__c
				FROM Contact
				WHERE Id = :u.ContactId
				];
			}
		} catch (Exception ex) {

		}
		NewCase nc = new NewCase();
		nc.record = new Case(
		Version__c = (cnt != null && cnt.AccountId != null) ? cnt.Account.EC_Production_Version__c : null
		);
		nc.picklistMap = RP_Utility.getPicklistValuesMapByRecordType(
		'Case', 'Community', new List<String> {
			'Type', 'Priority', 'Case_Topic__c', 'Is_this_a_change_in_Production__c'
			, 'Category__c', 'Severity__c', 'Service_Area__c', 'Service__c'
			, 'Request__c', 'Has_this_issue_occurred_before__c'
			, 'Payroll_impact_in_next_2_days__c', 'Issue_of_Widespread_nature__c'
			, 'Browser_s_affected__c', 'EmpCenter_upgraded_or_patched_last_month__c'
		});
		nc.helpMap = RP_Utility.getHelpTexts(
		'Case'
		, new List<String> {
			'Subject'
			, 'Description', 'Type', 'Priority', 'Category__c'
			, 'Customer_Reference_Number__c', 'Customer_Category__c', 'Does_issue_prevent_critical_business__c'
			, 'Case_Topic__c', 'Request__c', 'Service__c', 'ServiceArea__c'
			, 'Version__c', 'Has_this_issue_occurred_before__c, Is_System_Down__c'
			, 'Payroll_impact_in_next_2_days__c', 'Issue_of_Widespread_nature__c'
			, 'Browser_s_affected__c', 'EmpCenter_upgraded_or_patched_last_month__c'
		});
		return nc;
	}

	@AuraEnabled
	public static Case insertCase(Case newCase) {
		Id arID;
		arID = RP_Utility.getAssignmentRuleId();
		Database.DMLOptions dmlOpts = new Database.DMLOptions();
		dmlOpts.assignmentRuleHeader.assignmentRuleId= arID;

		try {
			newCase.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'Community'].Id;
			newCase.Origin = 'Portal';
			newCase.setOptions(dmlOpts);
			insert newCase;
		} catch(DMLException ex) {
			System.debug('INSERT CASE: [' + newCase + ']: ' + ex.getMessage());
			throw new AuraHandledException('Failed to open a new case.');
		}
		return newCase;
	}

	// Partner case related methods
	 @AuraEnabled
	 public static Account currentUsersAccount() {
	 	Id usersAccountId = [Select Contact.Account.Id From User Where Id =:UserInfo.getUserId()].Contact.Account.Id;
		Account acc = [Select Id, Name From Account Where Id =:usersAccountId];
		return acc;
	  }

	@AuraEnabled
	public static List<Account> getPartnerAccounts() {
		List<Account> accountList = RP_PartnerCaseController.fetchPartnerAccounts();
		return accountList;
	}

	@AuraEnabled
	public static List<Contact> getAccountContacts(String accountId) {
		List<Contact> contactList = RP_PartnerCaseController.getAccountContacts(accountId);
		return contactList;
	}

	@AuraEnabled
	public static Case insertPartnerCase(Case newCase, String accountId, String contactId) {
		// Id arID;
		// arID = RP_Utility.getAssignmentRuleId();
		// Database.DMLOptions dmlOpts = new Database.DMLOptions();
		// dmlOpts.assignmentRuleHeader.assignmentRuleId = arID;
		// try {
		// 	newCase.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'Community'].Id;
		// 	newCase.AccountId = accountId;
		// 	newCase.ContactId = contactId;
		// 	newCase.Origin = 'Portal';
		// 	newCase.setOptions(dmlOpts);
		// 	insert newCase;
		// } catch(DMLException ex) {
		// 	System.debug('INSERT CASE: [' + newCase + ']: ' + ex.getMessage());
		// 	throw new AuraHandledException('Failed to open a new case.');
		// }
		RP_PartnerCaseController.insertPartnerCase(newCase, accountId, contactId);
		return newCase;
	}

	// @AuraEnabled
	// public static boolean isPartnerUser() {
	// 	Id partner;
	// 	partner = [Select Id, name FROM Profile Where Name = 'Partner Community Plus User'].Id;
	// 	boolean isPartnerUser = false;
	// 	if(UserInfo.getProfileId() == partner) {
	// 		isPartnerUser = true;
	// 	}
	// 	return isPartnerUser;
	// }


	// Helptext and Picklist methods
	@AuraEnabled
	public static String getHelpText(String objectName, String fieldName) {
		return RP_Utility.getHelpText(objectName, fieldName);
	}

	@AuraEnabled
	public static List<String> getPicklistValues(String objectName, String fieldName) {
		return RP_Utility.getPicklistValues(objectName, fieldName);
	}

	@AuraEnabled
	public static List<String> getDependentPicklistValues(String objectName, String controllingField, String dependentField, String controllingValue) {
		return RP_Utility.getDependentPicklistValues(objectName, controllingField, dependentField, controllingValue);
	}

	@AuraEnabled
	public static List<String> getPicklistValuesByRecordType(String objectName, String recordTypeName, String fieldName) {
		return RP_Utility.getPicklistValuesByRecordType(objectName, recordTypeName, fieldName);
	}

	@AuraEnabled
	public static List<String> getDependentPicklistValuesByRecordType(String objectName, String recordTypeName, String fieldName, String fieldValue) {
		return RP_Utility.getDependentPicklistValuesByRecordType(objectName, recordTypeName, fieldName, fieldValue);
	}

	@AuraEnabled
	public static List<String> getCaseDependentPicklist(String fieldName, String fieldValue) {
		return RP_Utility.getDependentPicklistValuesByRecordType('Case', 'Community', fieldName, fieldValue);
	}

	public class NewCase {
		@AuraEnabled public Case record;
		@AuraEnabled public Map<String, List<String>> picklistMap;
		@AuraEnabled public Map<String, String> helpMap;
	}
}