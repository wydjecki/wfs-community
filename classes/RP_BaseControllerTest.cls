@isTest
private class RP_BaseControllerTest {

	@isTest static void test_getCurrentUser() {
		Id userId = UserInfo.getUserId();
		Test.startTest();
		RP_BaseController.getCurrentUser();
		Test.stopTest();
		System.assertNotEquals(null, userId);
	}

}