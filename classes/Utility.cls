public with sharing class Utility {
	private static Map<String,Integer> hexMap = new Map<String, Integer>{'0'=>0,'1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'A'=>10,'B'=>11,'C'=>12,'D'=>13,'E'=>14,'F'=>15,'a'=>10,'b'=>11,'c'=>12,'d'=>13,'e'=>14,'f'=>15};
	
	public static List<String> getDependentPicklistValues(String objectName, String controllingField, String dependentField, String controllingValue) {
		List<String> values = new List<String>();
		Schema.DescribeSObjectResult o = Schema.describeSObjects(new List<String> {objectName})[0];
		Schema.DescribeFieldResult f = o.fields.getMap().get(controllingField).getDescribe();
		Integer index = -1;
		Integer counter = 0;
		for (Schema.PicklistEntry pe : f.getPicklistValues()) {
			if (pe.value.toLowerCase() == controllingValue.toLowerCase()) {
				index = counter;
				break; 
			}
			counter++;
		}
		if (index == -1) return values;

		f = o.fields.getMap().get(dependentField).getDescribe();
		for (Schema.PicklistEntry pe : f.getPicklistValues()) {
			String js = JSON.serialize(pe);
			Map<String, String> jmap = (Map<String, String>) JSON.deserialize(js, Map<String, String>.class);
			String validFor = jmap.get('validFor');
			if (validFor != null && isDependentValue(index, validFor)) values.add(jmap.get('value')); 
		}

		return values;
	}


	public static Boolean isDependentValue(Integer parentValueIndex, String validFor) { 
		String decoded = EncodingUtil.convertToHex(EncodingUtil.base64Decode(validFor));
		Integer bits = hexToInt(decoded);
		return ((bits & (128>>Math.mod(parentValueIndex, 8)) ) != 0);
	}

	public static Integer hexToInt(String hex) {
		Integer result = 0;
		for(Integer i=0;i<hex.length();i+=2) {
			result += (hexMap.get(hex.substring(i,i+1)) * 16) + (hexMap.get(hex.substring(i+1,i+2)));
		}
		return result;
	}


}