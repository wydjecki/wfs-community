@isTest private class RP_DocumentControllerTest {
	@isTest static void currentUserTest() {
		RP_DocumentController.getCurrentUser();
	}

	@isTest static void topicTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentController.getTopics();
		Test.stopTest();
	}

	@isTest static void guessAccessLinkTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentController.getGuestAccessLink('path');
		Test.stopTest();
	}

	@isTest static void downloadTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentController.downloadFile('path');
		Test.stopTest();
	}

	@isTest static void search() {
		RP_DocumentController.SearchFilter f = new RP_DocumentController.SearchFilter();
		f.keyword = 'Find me';
		f.pageInfo = new Map<String, String> { 'StartRow'=>'0' , 'RowLimit'=>'10' };
		f.topicInfo = new Map<String, String> { 'Id'=>'xxx-xx-xxx-xx', 'Name'=>'Selected Topic' };
		f.refinements = new Map<String, List<String>>();
		f.refinements.put('Product', new List<String> { 'P1', 'xxx-xx-xxx-xx' });
		f.refinements.put('Topic', new List<String> { 'T1', 'xxx-xx-xxx-xx' });
		f.refinements.put('FileType', new List<String> { 'F1', 'xxx-xx-xxx-xx' });
		f.refinements.put('ModifiedDate', new List<String> { 'M1', 'xxx-xx-xxx-xx' });

		String filter = JSON.serialize(f);

		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		System.runAs([SELECT Id FROM User WHERE UserName = 'prttst1@tester.co' LIMIT 1][0]) {
			RP_DocumentController.search(filter);
		}
		Test.stopTest();
	}


	@testSetup static void setup() {
		insert new Community_Setting__c(
			SetupOwnerId = UserInfo.getOrganizationId()
			, Document_Topic_URL__c = 'https://document.topic.url'
			, Document_Product_URL__c = 'https://document.product.url'
			, Document_Base_Search_URL__c = 'https://document.base.search.url'
			, Document_Search_Refiners__c = 'RefinableCommunityAudience'
			, Document_Content_Type__c = '0x29302932039'
			, Document_Data_Set_GUID__c = '2492042-2492409-20492049'
		);

		Account act = new Account(
			Name = 'Test Account'
			, Document_Access__c = 'Customer;Reseller Partner'
			, Ecosystem__c = 'SAP;Cerner'
			, Product_Family__c = 'EmpLive'
		);
		insert act;

		Contact cnt = new Contact(
			FirstName = 'Tester'
			, LastName = 'Joe'
			, AccountId = act.Id
		);
		insert cnt;

		User u = new User(
			ProfileId = [SELECT Id FROM Profile WHERE Name = 'Partner Community Plus Admin User'].Id
			, LastName = 'Tester'
			, Email = 'tst1@tester.co'
			, UserName = 'prttst1@tester.co'
			, Alias = 'tst1'
			, TimeZoneSidKey = 'America/Chicago'
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_US'
			, ContactId = cnt.Id
		);
		insert u;
	}
}