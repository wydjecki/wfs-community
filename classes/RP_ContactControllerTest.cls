@isTest

private class RP_ContactControllerTest {

	@isTest static void test_getCommunicationPreferencesPos() {

		User u = [Select ContactId From User Where LastName = 'TestConUser' Limit 1];
		Contact c = [Select Id, Customer_Communication_Preference__c From Contact Where Id =:u.ContactId];
		System.debug('Contact Customer_Communication_Preference__c ======> ' + c.Customer_Communication_Preference__c);
		System.runAs(u){
		Test.startTest();
		try {
			RP_ContactController.getCommunicationPreferences();
		}catch(Exception ex){}
		Test.stopTest();
		}
	}

	@isTest static void test_getCommunicationPreferencesNeg() {
		User u = [Select ContactId From User Limit 1];
		System.runAs(u){
		Test.startTest();
		try {
			RP_ContactController.getCommunicationPreferences();
		}catch(Exception ex){
			System.assertNotEquals(null, ex.getMessage());
		}
		Test.stopTest();
		}
	}

	@isTest static void test_setCommunicationPreferences() {
		User u = [Select ContactId From User Where LastName = 'TestConUser' Limit 1];
		Contact c = [Select Id, Customer_Communication_Preference__c From Contact Where Id =:u.ContactId];
		test.startTest();
		try{
		RP_ContactController.setCommunicationPreferences('[{"TSB Notices" : null}]');
	}catch(exception ex) {}
		Test.stopTest();
	}


	@testSetup
	static void setup() {
		Account acc = new Account(name='Test Acc', EC_Production_Version__c = '1234');
		insert acc;
		System.assertEquals('Test Acc', acc.Name);

		Contact con = new Contact(lastname='Test Con', AccountId=acc.Id, Email='TestEmail@gmail.com', Customer_Communication_Preference__c = 'DCT TSB Notices;Product Update Notices');
		insert con;
		System.assertEquals('Test Con', con.LastName);


		Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Plus Login User - Custom'];
		Contact userCon = [SELECT Id, Customer_Communication_Preference__c FROM Contact Where LastName = 'Test Con' Limit 1];
			User us1 = new User(
			Alias = 'test1234'
			, Email='testuser@testorg1.com'
			, EmailEncodingKey='UTF-8'
			, LastName='TestConUser'
			, ContactId = userCon.Id
			, LanguageLocaleKey='en_US'
			, LocaleSidKey='en_US'
			, ProfileId = p.Id
			, TimeZoneSidKey='America/Los_Angeles'
			, UserName='guestusermanager@testorg.com'
			);
			insert us1;
			System.assertEquals('TestConUser', us1.LastName);
			System.debug('Contact Com Preference: ' + userCon.Customer_Communication_Preference__c);
		}
}