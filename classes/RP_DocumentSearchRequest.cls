public class RP_DocumentSearchRequest {
	public String QueryText { get; set; }
	public Boolean EnableStemming { get; set; }
    public Boolean EnablePhonetic { get; set; }
    public Boolean EnableNicknames { get; set; }
    public Boolean TrimDuplicates { get; set; }
    public Boolean EnableFql { get; set; }
    public Boolean EnableQueryRules { get; set; }
    public Boolean ProcessBestBets { get; set; }
    public Boolean ByPassResultTypes { get; set; }
    public Boolean ProcessPersonalFavorites { get; set; }
    public Boolean GenerateBlockRankLog { get; set; }
    public Boolean IncludeRankDetail { get; set; }
    public Integer StartRow { get; set; }
    public Integer RowLimit { get; set; }
    public Integer RowsPerPage { get; set; }
    public String SelectProperties { get; set; }
    public String GraphQuery { get; set; }
    public String GraphRankingModel { get; set; }
    public String Refiners { get; set; }
    public String RefinementFilters { get; set; }
    public String HitHighlightedProperties { get; set; }
    public String RankingModelId { get; set; }
    public String SortList { get; set; }
    public String Culture { get; set; }
    public String SourceId { get; set; }
    public String HiddenConstraints { get; set; }
    public String ResultsUrl { get; set; }
    public String QueryTag { get; set; }
    public String CollapseSpecification { get; set; }
    public String QueryTemplate { get; set; }
    public Long TrimDuplicatesIncludeId { get; set; }
    public String ClientType { get; set; }
    public String PersonalizationData { get; set; }	

    public String generateHttpGetUriPart() {
    	String uri = '';
        //System.debug('QueryText: ' + QueryText);
    	uri += 'querytext=\'' + EncodingUtil.UrlEncode(QueryText, 'UTF-8') + '\'';
		uri += '&enablestemming=' + (EnableStemming ? 'true' : 'false');
    	uri += '&enablephonetic=' + (EnablePhonetic ? 'true' : 'false');
    	uri += '&enablenicknames=' + (EnableNicknames ? 'true' : 'false');
    	uri += '&trimduplicates=' + (TrimDuplicates ? 'true' : 'false');
    	uri += '&enablefql=' + (EnableFql ? 'true' : 'false');
    	uri += '&enablequeryrules=' + (EnableQueryRules ? 'true' : 'false');
    	uri += '&processbestbets=' + (ProcessBestBets ? 'true' : 'false');
    	uri += '&bypassresulttypes=' + (ByPassResultTypes ? 'true' : 'false');
    	uri += '&processpersonalfavorites=' + (ProcessPersonalFavorites ? 'true' : 'false');
    	uri += '&generateblockranklog=' + (GenerateBlockRankLog ? 'true' : 'false');
    	//uri += '&includerankdetail=' + (IncludeRankDetail ? 'true' : 'false');

        System.debug('StartRow: ' + StartRow);
        System.debug('RowsPerPage: ' + RowsPerPage);
        System.debug('RowLimit: ' + RowLimit);
        System.debug('TrimDuplicatesIncludeId: ' + TrimDuplicatesIncludeId);

        if (StartRow != null && StartRow >= 0) {
            uri += '&startrow=' + String.valueOf(StartRow);
        }
        if (RowLimit != null && RowLimit >= 0) {
            uri += '&rowlimit=' + String.valueOf(RowLimit);
        }
        if (RowsPerPage != null && RowsPerPage >= 0) {
            uri += '&rowsperpage=' + String.valueOf(RowsPerPage);
        }
        if (TrimDuplicatesIncludeId != null) {
            uri += '&trimduplicatesincludeid=' + String.valueOf(TrimDuplicatesIncludeId);
        }

	    uri += (String.isNotBlank(Refiners)? '&refiners=\'' + EncodingUtil.urlEncode(Refiners, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(RefinementFilters)? '&refinementfilters=\'' + EncodingUtil.urlEncode(RefinementFilters, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(HitHighlightedProperties)? '&hithighlightedproperties=\'' + EncodingUtil.urlEncode(HitHighlightedProperties, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(RankingModelId)? '&rankingmodelid=\'' + EncodingUtil.urlEncode(RankingModelId, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(Culture)? '&culture=\'' + EncodingUtil.urlEncode(Culture, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(HiddenConstraints)? '&hiddenconstraints=\'' + EncodingUtil.urlEncode(HiddenConstraints, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(ResultsUrl)? '&resultsurl=\'' + EncodingUtil.urlEncode(ResultsUrl, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(QueryTag)? '&querytag=\'' + EncodingUtil.urlEncode(QueryTag, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(CollapseSpecification)? '&collapsespecification=\'' + EncodingUtil.urlEncode(CollapseSpecification, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(PersonalizationData)? '&personalizationdata=\'' + EncodingUtil.urlEncode(PersonalizationData, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(ClientType)? '&clienttype=\'' + EncodingUtil.urlEncode(ClientType, 'UTF-8') + '\'' : '');
	    uri += (String.isNotBlank(QueryTemplate)? '&querytemplate=\'' + EncodingUtil.urlEncode(QueryTemplate, 'UTF-8') + '\'' : '');

	    uri += getSourceAttribute('');
	    uri += getSortAttribute('');

	    if (IncludeRankDetail) {
	    	SelectProperties = 'rankdetail,title,path,language,workid';
	    }
	    uri += (String.isNotBlank(SelectProperties)? '&selectproperties=\'' + EncodingUtil.urlEncode(SelectProperties, 'UTF-8') + '\'' : '');
    	return uri;
    }

    public String generateHttpPostBodyPart() {
    	String body = '';

        body += '{"request":{"QueryText":"' + QueryText + '"';

		body += ', "EnableStemming":' + (EnableStemming ? 'true':'false'); 
    	body += ', "EnablePhonetic":' + (EnablePhonetic ? 'true':'false'); 
    	body += ', "EnableNicknames":' + (EnableNicknames ? 'true':'false'); 
    	body += ', "TrimDuplicates":' + (TrimDuplicates ? 'true':'false'); 
    	body += ', "EnableFql":' + (EnableFql ? 'true':'false'); 
    	body += ', "EnableQueryRules":' + (EnableQueryRules ? 'true':'false'); 
    	body += ', "ProcessBestBets":' + (ProcessBestBets ? 'true':'false'); 
    	body += ', "ByPassResultTypes":' + (ByPassResultTypes ? 'true':'false'); 
    	body += ', "ProcessPersonalFavorites":' + (ProcessPersonalFavorites ? 'true':'false'); 
    	body += ', "GenerateBlockRankLog":' + (GenerateBlockRankLog ? 'true':'false'); 
    	//body += ', "IncludeRankDetail":' + (IncludeRankDetail ? 'true':'false'); 

    	body += (StartRow != null && StartRow >= 0 ? ', "StartRow":' + StartRow : '');
    	body += (RowsPerPage != null && RowsPerPage >= 0 ? ', "RowsPerPage":' + RowsPerPage : '');
    	body += (RowLimit != null && RowLimit >= 0 ? ', "RowLimit":' + RowLimit : '');
    	body += (TrimDuplicatesIncludeId != null ? ', "TrimDuplicatesIncludeId":' + TrimDuplicatesIncludeId : '');

	    body += (String.isNotBlank(Refiners)? ', "Refiners":"' + Refiners + '"' : '');
	    body += (String.isNotBlank(RankingModelId)? ', "RankingModelId":"' + RankingModelId + '"' : '');
	    body += (String.isNotBlank(Culture)? ', "Culture":"' + Culture + '"' : '');
	    body += (String.isNotBlank(HiddenConstraints)? ', "HiddenConstraints":"' + HiddenConstraints + '"' : '');
	    body += (String.isNotBlank(ResultsUrl)? ', "ResultsUrl":"' + ResultsUrl + '"' : '');
	    body += (String.isNotBlank(QueryTag)? ', "QueryTag":"' + QueryTag + '"' : '');
	    body += (String.isNotBlank(CollapseSpecification)? ', "CollapseSpecification":"' + CollapseSpecification + '"' : '');
	    body += (String.isNotBlank(PersonalizationData)? ', "PersonalizationData":"' + PersonalizationData + '"' : '');
	    body += (String.isNotBlank(ClientType)? ', "ClientType":"' + ClientType + '"' : '');
	    body += (String.isNotBlank(QueryTemplate)? ', "QueryTemplate":"' + QueryTemplate + '"' : '');


	    body += (String.isNotBlank(RefinementFilters)? ', "RefinementFilters":{"results":["' + RefinementFilters + '"]}' : '');
	    body += (String.isNotBlank(HitHighlightedProperties)? ', "HitHighlightedProperties":{"results":["' + HitHighlightedProperties + '"]}' : '');
 
 		body += getSourceAttribute('JSON');
 		body += getSortAttribute('JSON');
	
	    if (IncludeRankDetail) {
	    	SelectProperties = 'rankdetail,title,path,language,workid';
	    }
	    body += (String.isNotBlank(SelectProperties)? ', "SelectProperties":{"results":["' + SelectProperties + '"]}' : '');


	    body += '}}';

    	return body;
    }

    @testVisible
    private String getSourceAttribute(String type) {
    	String result = '';
    	if (String.isNotBlank(SourceId)) {
    		List<String> tokens = SourceId.split(':|\\|');
    		if (tokens.size() == 2) {
    			if (type == 'JSON') {
	    			result += ', "Properties":{"results":["SourceLevel:' + tokens[0] + ',SourceName:' + tokens[1] + '"]}'; 
    			} else {
    				result += '&properties=\'' + EncodingUtil.urlEncode('SourceLevel:' + tokens[0] + ',SourceName:' + tokens[1], 'UTF-8') + '\''; 
    			}
			} else {
				if (type == 'JSON') {
		    		result += ', "SourceId":"' + SourceId + '"';
				} else {
	    			result += '&sourceid=\'' + EncodingUtil.urlEncode(SourceId, 'UTF-8') + '\'';
				}
			}
    	}
    	return result;
    }

    @testVisible
    private String getSortAttribute(String type) {
    	String result = '';

    	if (String.isNotBlank(SortList)) {
    		if (type == 'JSON') {
		    	List<String> sortBody = new List<String>();
		    	List<String> tokens = SortList.split(',');
		    	System.debug('SORT TOKENS: ' + tokens);
		    	for (Integer i=0; i<tokens.size(); i++) {
		    		List<String> sortTokens = tokens[i].split(':');
		    		if (sortTokens.size() == 2) {
	    				sortBody.add('{"Property":"' + sortTokens[0] 
	    						+ '","Direction":' + (sortTokens[1].toLowerCase() == 'descending'? '1':'0') 
	    						+ '}'
	    				);
	    			} else {
	    				sortBody.add('{"Property":"' + sortTokens[0] + '","Direction":0}');
	    			}
	    			System.debug('SORT BODY: ' + sortBody);
		    	}
	            result += ', "SortList":{"results":[' + String.join(sortBody, ',') + ']}';
            } else {
            	result += '&sortlist=\'' + EncodingUtil.urlEncode(SortList, 'UTF-8') + '\'';
            }
    	}
    	System.debug('RESULT: ' + result);
    	return result;
    }
}