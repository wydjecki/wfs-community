@isTest
private class RP_IdeaControllerTest {

	@isTest static void test_getIdeaList() {
        String STATUS_CLOSED = RP_IdeaController.STATUS_CLOSED;
		Test.startTest();
		RP_IdeaController.getIdeaList('test', 'undelivered', 1, 1);
        RP_IdeaController.getIdeaList('test', STATUS_CLOSED, 1, 1);
		Test.stopTest();
	}
	@isTest static void test_getIdeaList1() {
			Test.startTest();
			RP_IdeaController.getIdeaList(null, 'undelivered', 1, 1);
			Test.stopTest();
	}
	@isTest static void test_getIdeaList2() {
			Test.startTest();
        String STATUS_CLOSED = RP_IdeaController.STATUS_CLOSED;
			RP_IdeaController.getIdeaList(null, STATUS_CLOSED , 1, 1);
			Test.stopTest();
	}

	@isTest static void test_getIdeaListOLD() {
		Test.startTest();
		RP_IdeaController.getIdeaListOLD('test', 'undelivered', 1, 1);
		Test.stopTest();
	}
	
	@isTest static void test_getIdeaListOLD1() {
		Test.startTest();
		RP_IdeaController.getIdeaListOLD(null, 'undelivered', 1, 1);
		Test.stopTest();
	}
	
	@isTest static void test_getIdeasOld() {
		Test.startTest();
		RP_IdeaController.getIdeasOLD(null, 'undelivered', 1, 1);
		Test.stopTest();
	}
	
	@isTest static void test_getIdeasOld1() {
		Test.startTest();
		RP_IdeaController.getIdeasOLD('test', 'undelivered', 1, 1);
		Test.stopTest();
	}

	@isTest static void test_getTrendingIdeas() {
		Test.startTest();
		RP_IdeaController.getTrendingIdeas();
		Test.stopTest();
	}

	@isTest static void test_getIdea() {
		String ideaRec;
		ideaRec = [
				SELECT Id, Status, Title, CreatedDate, CreatedById
				, Categories, CreatorSmallPhotoUrl, CreatorName, NumComments
				, VoteTotal, Body, Account__c
				FROM Idea
				WHERE Title = 'Test Idea123'].Id;
		Test.startTest();
        try{
			RP_IdeaController.getIdea(ideaRec);
        }catch(Exception ex) {}
		Test.stopTest();
	}

	@isTest static void test_setIdea() {
		Account acc = new Account(name='Test');
		insert acc;
		Idea newIdea = new Idea(Title = 'Test', Account__c = acc.Id);
		Test.startTest();
        try{
			RP_IdeaController.setIdea(newIdea);
        }catch(Exception ex) {}
		Test.stopTest();
	}

	@isTest static void test_getComments() {
		id ideaRec;
 		ideaRec = [SELECT Id FROM Idea WHERE Title = 'Test Idea123'].Id;
        Test.startTest();
        try{
		RP_IdeaController.getComments(ideaRec);
        }catch(Exception ex) {}
		Test.stopTest();
	}

	@isTest static void test_setComment() {
		String ideaRec;
		ideaRec = [SELECT Id FROM Idea WHERE Title = 'Test Idea123'].Id;
        Test.startTest();
        try{
        RP_IdeaController.setComment(ideaRec, null);
        RP_IdeaController.setComment(ideaRec, 'Test comment');
        }catch(Exception ex) {}
        Test.stopTest();
    }
	
    @isTest static void test_voteIdea() {
		String ideaRec;
		ideaRec = [SELECT Id FROM Idea WHERE Title = 'Test Idea123'].Id;
            Test.startTest();
        try{
            RP_IdeaController.voteIdea(ideaRec, 'Up');
            Test.stopTest();
            List<Vote> votes = [Select Id From Vote Where parentId=:ideaRec];
        }catch(Exception ex){}
	}
    
    @TestSetup static void ideaSetup() {
        Id communityId = [Select Id From Community Where name='Internal Ideas'].Id;
        
        Idea newIdea = new idea();
        newIdea.Title = 'Test Idea123';
        newIdea.CommunityId = communityId;
        insert newIdea;
        System.assertEquals('Test Idea123', newIdea.Title);
        
        IdeaComment ic = new IdeaComment();
        ic.IdeaId = newIdea.Id;
        ic.CommentBody= 'Test Idea Comment';
        insert ic;
        System.assertEquals('Test Idea Comment', ic.CommentBody);
        
        Vote vi = new Vote();
        vi.parentId = newIdea.Id;
        vi.Type = 'Down';
        try{
        insert vi;
        }catch(Exception ex){}
    } 
}