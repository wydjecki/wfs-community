@isTest private class UtilityServiceTest {
	@isTest static void BasicClasses() {
		UtilityService.Soap s = new UtilityService.Soap();
		UtilityService.GetUserInfoResult guir = new UtilityService.GetUserInfoResult();
		UtilityService.LoginResult lr = new UtilityService.LoginResult();
		UtilityService.loginResponse_element lre = new UtilityService.loginResponse_element();
		UtilityService.login_element le = new UtilityService.login_element();

		UtilityService.Port p = new UtilityService.Port();
		UtilityService.Metadata m = new UtilityService.Metadata();
		UtilityService.readMetadata_element rme = new UtilityService.readMetadata_element();
		UtilityService.readMetadataResponse_element rmre = new UtilityService.readMetadataResponse_element();
		UtilityService.ReadResult rr = new UtilityService.ReadResult();
		UtilityService.SessionHeader_element she = new UtilityService.SessionHeader_element();
		UtilityService.LookupFilter lf = new UtilityService.LookupFilter();
		UtilityService.FilterItem fi = new UtilityService.FilterItem();
		UtilityService.Picklist pl = new UtilityService.Picklist();
		UtilityService.PicklistValue plv = new UtilityService.PicklistValue();
		UtilityService.ReadCustomFieldResult rcfr = new UtilityService.ReadCustomFieldResult();
		UtilityService.ReadRecordTypeResult rrtr = new UtilityService.ReadRecordTypeResult();
		UtilityService.RecordType rt = new UtilityService.RecordType();
		UtilityService.RecordTypePicklistValue rtplv = new UtilityService.RecordTypePicklistValue();
		UtilityService.CustomField cfe = new UtilityService.CustomField();
		UtilityService.readCustomFieldResponse_element rcfre = new UtilityService.readCustomFieldResponse_element();
		UtilityService.readRecordTypeResponse_element rrtre = new UtilityService.readRecordTypeResponse_element();
	}

	@isTest static void MethodCalls() {
		Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		UtilityService.Soap s = new UtilityService.Soap();
		s.login('login', 'password');

		Test.setMock(WebServiceMock.class, new UtilityServiceReadMetadataMock());
		UtilityService.Port p = new UtilityService.Port();
		p.readMetadata('RecordType', new List<String> {'name'});

	}
}