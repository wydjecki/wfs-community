public class RP_CommunityLinks {

	@AuraEnabled
	public static Community_Link__c[] getComLinks() {
		return [SELECT Id, Name, Description__c, URL__c FROM Community_Link__c];
	}
}