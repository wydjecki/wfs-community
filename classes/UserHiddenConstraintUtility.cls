public with sharing class UserHiddenConstraintUtility {

	private static final string COMMUNITY_DOCUMENT_CT_ID = 'ContentTypeId:0x01010045287B932D1C4739A0C406ADC0B4048A01*';

	public static string createPartnerEcosystemConstraint(String semiColonSeparatedEcosystems) {
		
			List<String> ecosystems;
			if (semiColonSeparatedEcosystems.contains(';')) {
				ecosystems = semiColonSeparatedEcosystems.split(';');
			} else ecosystems = new List<String> {};

		 	String ecosystemConstraint = '';

            for (Integer i = 0; i < ecosystems.size(); i++)
            {                
                if (i == ecosystems.size() - 1)
                {
                    ecosystemConstraint += 'RefinablePartnerEcosystem:' + ecosystems[i];
                }
                else
                {
    
                    ecosystemConstraint += 'RefinablePartnerEcosystem:' + ecosystems[i] + ' OR ';
                }
                
            }

            ecosystemConstraint += ' OR RefinablePartnerEcosystem:WorkForce';

            if (ecosystems.size() == 0)
            {
                ecosystemConstraint = 'RefinablePartnerEcosystem:WorkForce';
            }

            if (ecosystems.size() == 1 && ecosystems[0] == '')
            {
                ecosystemConstraint = 'RefinablePartnerEcosystem:WorkForce';
            }
            

            return '(' + ecosystemConstraint + ')';
	}

	 public static string createPartnerTypeConstraints(String semiColonSeparatedPartnerTypes)
        {
			List<String> partnerTypes;
			if (semiColonSeparatedPartnerTypes.contains(';')) {
				partnerTypes = semiColonSeparatedPartnerTypes.split(';');
			} else partnerTypes = new List<String> {};

            string partnerTypesConstraint = '';

            for (Integer i = 0; i < partnerTypes.size(); i++)
            {
                if (i == partnerTypes.size() - 1)
                {
                    partnerTypesConstraint += 'RefinableWhoCanView:' + partnerTypes[i];
                }
                else
                {

                    partnerTypesConstraint += 'RefinableWhoCanView:' + partnerTypes[i] + ' OR ';
                }

            }

            return '(' + partnerTypesConstraint + ')';

        }

		public static string createFullPartnerConstraint(String ecosystemConstraints, String partnerTypeConstraints){
			// this just adds an AND between these constraints and also appends the Content Type ID to the end
			// we need to add the content type ID to the end for security reasons
			return ecosystemConstraints + ' AND ' + partnerTypeConstraints + ' AND ' + COMMUNITY_DOCUMENT_CT_ID;
		}

}