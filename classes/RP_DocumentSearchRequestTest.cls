@isTest private class RP_DocumentSearchRequestTest {
	@isTest static void getHttpGetRequestTest() {
		RP_DocumentSearchRequest req = new RP_DocumentSearchRequest();
		req.QueryText = 'Hello';
		req.EnableStemming = true;
    	req.EnablePhonetic = false;
    	req.EnableNicknames = true;
    	req.TrimDuplicates = false;
    	req.EnableFql = true;
    	req.EnableQueryRules = false;
    	req.ProcessBestBets = true;
    	req.ByPassResultTypes = false;
    	req.ProcessPersonalFavorites = true;
    	req.GenerateBlockRankLog = false;
    	req.IncludeRankDetail = true;
    	req.StartRow = 1;
    	req.RowLimit = 20;
    	req.RowsPerPage = 20;
    	req.SelectProperties = 'title';
    	req.GraphQuery = '';
    	req.GraphRankingModel = '';
    	req.Refiners = 'Title,Path';
    	req.RefinementFilters = 'LastModifiedDate:range(min,today)';
    	req.HitHighlightedProperties = 'This is the summary of the document';
    	req.RankingModelId = 'ID-OF-RANKING-MODEL';
    	req.SortList = 'Title:descending,Path:ascending';
    	req.Culture = 'en_US';
    	req.SourceId = 'GUID-OF-SOURCE-ID';
    	req.HiddenConstraints = 'UserType:Standard';
    	req.ResultsUrl = 'https://ResultsUrl';
    	req.QueryTag = 'Document,Important';
    	req.CollapseSpecification = '012345';
    	req.QueryTemplate = 'TEMPLATE';
    	req.TrimDuplicatesIncludeId = 987654321;
    	req.ClientType = 'Non-Admin';
    	req.PersonalizationData = 'My Customization';

    	String result = req.generateHttpGetUriPart();
    	System.assert(String.isNotBlank(result));

    	List<String> attributes = result.split('&');
    	Map<String, String> attributeMap = new Map<String, String>();
    	for (String attribute : attributes) {
    		List<String> parts = attribute.split('=');
    		attributeMap.put(parts[0].toLowerCase(), parts[1]);
    	}
		System.assert(attributeMap.containsKey('querytext'), attributeMap);
		System.assert(attributeMap.containsKey('enablestemming'), attributeMap);
    	System.assert(attributeMap.containsKey('enablephonetic'), attributeMap);
    	System.assert(attributeMap.containsKey('enablenicknames'), attributeMap);
    	System.assert(attributeMap.containsKey('trimduplicates'), attributeMap);
    	System.assert(attributeMap.containsKey('enablefql'), attributeMap);
    	System.assert(attributeMap.containsKey('enablequeryrules'), attributeMap);
    	System.assert(attributeMap.containsKey('processbestbets'), attributeMap);
    	System.assert(attributeMap.containsKey('bypassresulttypes'), attributeMap);
    	System.assert(attributeMap.containsKey('processpersonalfavorites'), attributeMap);
    	System.assert(attributeMap.containsKey('generateblockranklog'), attributeMap);
    	//System.assert(attributeMap.containsKey('includerankdetail'), attributeMap);
    	System.assert(attributeMap.containsKey('startrow'), attributeMap);
    	System.assert(attributeMap.containsKey('rowlimit'), attributeMap);
    	System.assert(attributeMap.containsKey('rowsperpage'), attributeMap);
    	System.assert(attributeMap.containsKey('selectproperties'), attributeMap);
    	//System.assert(attributeMap.containsKey('GraphQuery'), attributeMap);
    	//System.assert(attributeMap.containsKey('GraphRankingModel'), attributeMap);
    	System.assert(attributeMap.containsKey('refiners'), attributeMap);
    	System.assert(attributeMap.containsKey('refinementfilters'), attributeMap);
    	System.assert(attributeMap.containsKey('hithighlightedproperties'), attributeMap);
    	System.assert(attributeMap.containsKey('rankingmodelid'), attributeMap);
    	System.assert(attributeMap.containsKey('sortlist'), attributeMap);
    	System.assert(attributeMap.containsKey('culture'), attributeMap);
    	System.assert(attributeMap.containsKey('sourceid'), attributeMap);
    	System.assert(attributeMap.containsKey('hiddenconstraints'), attributeMap);
    	System.assert(attributeMap.containsKey('resultsurl'), attributeMap);
    	System.assert(attributeMap.containsKey('querytag'), attributeMap);
    	System.assert(attributeMap.containsKey('collapsespecification'), attributeMap);
    	System.assert(attributeMap.containsKey('querytemplate'), attributeMap);
    	System.assert(attributeMap.containsKey('trimduplicatesincludeid'), attributeMap);
    	System.assert(attributeMap.containsKey('clienttype'), attributeMap);
    	System.assert(attributeMap.containsKey('personalizationdata'), attributeMap);	
    }

	@isTest static void getHttpPostRequestTest() {
		RP_DocumentSearchRequest req = new RP_DocumentSearchRequest();
		req.QueryText = 'Hello';
		req.EnableStemming = true;
    	req.EnablePhonetic = false;
    	req.EnableNicknames = true;
    	req.TrimDuplicates = false;
    	req.EnableFql = true;
    	req.EnableQueryRules = false;
    	req.ProcessBestBets = true;
    	req.ByPassResultTypes = false;
    	req.ProcessPersonalFavorites = true;
    	req.GenerateBlockRankLog = false;
    	req.IncludeRankDetail = true;
    	req.StartRow = 1;
    	req.RowLimit = 20;
    	req.RowsPerPage = 20;
    	req.SelectProperties = 'title';
    	req.GraphQuery = '';
    	req.GraphRankingModel = '';
    	req.Refiners = 'Title,Path';
    	req.RefinementFilters = 'LastModifiedDate:range(min,today)';
    	req.HitHighlightedProperties = 'This is the summary of the document';
    	req.RankingModelId = 'ID-OF-RANKING-MODEL';
    	req.SortList = 'Title:descending';
    	req.Culture = 'en_US';
    	req.SourceId = 'GUID-OF-SOURCE-ID:X-123-456';
    	req.HiddenConstraints = 'UserType:Standard';
    	req.ResultsUrl = 'https://ResultsUrl';
    	req.QueryTag = 'Document,Important';
    	req.CollapseSpecification = '012345';
    	req.QueryTemplate = 'TEMPLATE';
    	req.TrimDuplicatesIncludeId = 987654321;
    	req.ClientType = 'Non-Admin';
    	req.PersonalizationData = 'My Customization';

    	String result = req.generateHttpPostBodyPart();
    	System.debug('POST: ' + result);

    	Map<String, Object> requestMap = (Map<String, Object>) JSON.deserializeUntyped(result);
    	Map<String, Object> attributeMap = (Map<String, Object>) requestMap.get('request');

		System.assert(attributeMap.containsKey('QueryText'), attributeMap);
		System.assert(attributeMap.containsKey('EnableStemming'), attributeMap);
    	System.assert(attributeMap.containsKey('EnablePhonetic'), attributeMap);
    	System.assert(attributeMap.containsKey('EnableNicknames'), attributeMap);
    	System.assert(attributeMap.containsKey('TrimDuplicates'), attributeMap);
    	System.assert(attributeMap.containsKey('EnableFql'), attributeMap);
    	System.assert(attributeMap.containsKey('EnableQueryRules'), attributeMap);
    	System.assert(attributeMap.containsKey('ProcessBestBets'), attributeMap);
    	System.assert(attributeMap.containsKey('ByPassResultTypes'), attributeMap);
    	System.assert(attributeMap.containsKey('ProcessPersonalFavorites'), attributeMap);
    	System.assert(attributeMap.containsKey('GenerateBlockRankLog'), attributeMap);
    	//System.assert(attributeMap.containsKey('IncludeRankDetail'), attributeMap);
    	System.assert(attributeMap.containsKey('StartRow'), attributeMap);
    	System.assert(attributeMap.containsKey('RowLimit'), attributeMap);
    	System.assert(attributeMap.containsKey('RowsPerPage'), attributeMap);
    	System.assert(attributeMap.containsKey('SelectProperties'), attributeMap);
    	//System.assert(attributeMap.containsKey('GraphQuery'), attributeMap);
    	//System.assert(attributeMap.containsKey('GraphRankingModel'), attributeMap);
    	System.assert(attributeMap.containsKey('Refiners'), attributeMap);
    	System.assert(attributeMap.containsKey('RefinementFilters'), attributeMap);
    	System.assert(attributeMap.containsKey('HitHighlightedProperties'), attributeMap);
    	System.assert(attributeMap.containsKey('RankingModelId'), attributeMap);
    	System.assert(attributeMap.containsKey('SortList'), attributeMap);
    	System.assert(attributeMap.containsKey('Culture'), attributeMap);
    	System.assert(attributeMap.containsKey('Properties'), attributeMap); //instead of SourceID
    	System.assert(attributeMap.containsKey('HiddenConstraints'), attributeMap);
    	System.assert(attributeMap.containsKey('ResultsUrl'), attributeMap);
    	System.assert(attributeMap.containsKey('QueryTag'), attributeMap);
    	System.assert(attributeMap.containsKey('CollapseSpecification'), attributeMap);
    	System.assert(attributeMap.containsKey('QueryTemplate'), attributeMap);
    	System.assert(attributeMap.containsKey('TrimDuplicatesIncludeId'), attributeMap);
    	System.assert(attributeMap.containsKey('ClientType'), attributeMap);
    	System.assert(attributeMap.containsKey('PersonalizationData'), attributeMap);	

    }

    @isTest static void getSourceAttributeTest() {
		RP_DocumentSearchRequest req = new RP_DocumentSearchRequest();
		req.SourceId = 'SOURCE-ID:X-123-456';
		String result = req.getSourceAttribute('');
		System.assertEquals('&properties=\'' + EncodingUtil.urlEncode('SourceLevel:SOURCE-ID,SourceName:X-123-456', 'UTF-8') + '\'', result);
		result = req.getSourceAttribute('JSON');
		System.assertEquals(', "Properties":{"results":["SourceLevel:SOURCE-ID,SourceName:X-123-456"]}', result);

		req.SourceId = 'SOURCE-ID-GUID';
		result = req.getSourceAttribute('');
		System.assertEquals('&sourceid=\'' + EncodingUtil.urlEncode('SOURCE-ID-GUID', 'UTF-8') + '\'', result);
		result = req.getSourceAttribute('JSON');
		System.assertEquals(', "SourceId":"SOURCE-ID-GUID"', result);
    }

    @isTest static void getSortAttributeTest() {
		RP_DocumentSearchRequest req = new RP_DocumentSearchRequest();
		req.SortList = 'Title:descending,Name';
		String result = req.getSortAttribute('');
		System.assertEquals('&sortlist=\'' + EncodingUtil.urlEncode('Title:descending,Name', 'UTF-8') + '\'', result);
		result = req.getSortAttribute('JSON');
		System.assertEquals(', "SortList":{"results":[{"Property":"Title","Direction":1},{"Property":"Name","Direction":0}]}', result);
    }

}