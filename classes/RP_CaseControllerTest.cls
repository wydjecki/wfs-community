@isTest(SeeAllData=true)
private class RP_CaseControllerTest {

	@isTest static void test_cases() {
		Id comId;
		comId = [Select Id From RecordType Where sObjectType = 'Case' AND DeveloperName = 'Community'].Id;
		Case cs = [Select Id From Case Where Status != 'Closed' Limit 1];
		Case nc = new Case(Status='Closed');

        Test.startTest();
		RP_CaseController.closeCase(cs.Id);
		RP_CaseController.caseStillOpen(cs.Id);
		RP_CaseController.getCase(cs.Id);
		RP_CaseController.getCase(null);
		RP_CaseController.insertCase(nc);
        try{
            RP_CaseController.closeCase(nc.Id);
        }catch(Exception ex) {
            System.assert(ex.getMessage().contains('Failed to'));
        }
        Test.stopTest();
	}

	 @isTest static void test_picklists() {
		 RP_CaseController.getPicklistValues('Case', 'Type');

		 Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		 try{
		 RP_CaseController.getDependentPicklistValues('Case', 'Service__c', 'Request__c', 'Professional');
	 		} catch (Exception ex) {}

		 Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		 try{
		 	RP_CaseController.getPicklistValuesByRecordType('Case', 'Community', 'Service__c');
	 		}catch(exception ex) {}

		 Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		 try{
		 RP_CaseController.getDependentPicklistValuesByRecordType('Case', 'Community', 'Service__c', 'Professional');
		 }catch(exception ex) {}

		 Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		 try{
		 RP_CaseController.getCaseDependentPicklist('Service__c', 'Professional');
 			}catch(exception ex) {}

		 Case newCase = [Select Id, Subject, Description, Type, Priority, Category__c From Case Limit 1];
		 Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		 try{
		 RP_CaseController.getNewCase();
	 }catch(exception ex) {}
	}

	@isTest static void test_helpText() {
		 RP_CaseController.getHelpText('Case', 'Type');
		 Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		 try{
		 RP_Utility.getHelpTexts('Case', new List<String>{'Subject'});
		 }catch(exception ex) {}
	}

	@isTest static void test_myCases() {
		try{
		RP_CaseController.getMyOpenCases('CreatedDate', 'DESC');
		}catch(Exception ex) {}
	}

	@isTest static void test_partnerCases() {
		Account acc = new Account(name='Test Acc', EC_Production_Version__c = '1234');
		insert acc;
		System.assertEquals('Test Acc', acc.Name);

		Contact con = new Contact(lastname='Test Con', AccountId=acc.Id, Email='TestEmail@gmail.com');
		insert con;
		System.assertEquals('Test Con', con.LastName);
		Case newCase = new Case(Status = 'Closed');
		Case failCase = new Case(Status = 'Test');
		String accId = [Select Id From Account Where Name = 'Test Acc' And EC_Production_Version__c = '1234' Limit 1].Id;
		String conId = [Select Id From Contact Where Email = 'TestEmail@gmail.com' Limit 1].Id;

		Test.startTest();
			try{
			RP_CaseController.currentUsersAccount();
			RP_CaseController.insertPartnerCase(failCase, accId, conId);
			}catch(Exception ex) {}

			RP_CaseController.getPartnerAccounts();
			RP_CaseController.getAccountContacts(accId);
			RP_CaseController.insertPartnerCase(newCase, accId, conId);
		Test.stopTest();
	}

}