@isTest
private class RP_CaseSurveyControllerTest {

	@isTest static void test_getSurvey() {
		Id cs1;
		List<Case> caseList = [Select Id From Case Where Status = 'Closed' Limit 1];
		if(caseList.size() > 0) {
			cs1 = caseList[0].Id;
		}
		List<Contact> conList = [Select Id From Contact Where LastName = 'Test Con' Limit 1];
		Id con;
		if(conList.size() > 0) {
			con = conList[0].Id;
		}
		Case_Survey__c cs = [Select Id from Case_Survey__c Limit 1];
		Test.startTest();
		Case_Survey__c csurvey = RP_CaseSurveyController.getSurvey(cs1, con);
		Test.stopTest();
		System.assertNotEquals(null, csurvey);
	}

	@isTest static void test_saveSurveyPos() {
		Case_Survey__c cs = [Select Id from Case_Survey__c Limit 1];
		Test.startTest();
		try {
			RP_CaseSurveyController.saveSurvey(cs);
		}catch(Exception ex) {
			System.assertNotEquals(null, ex);
		}

		Test.stopTest();
	}

	@isTest static void test_saveSurveyNeg() {
		Case_Survey__c cs = new Case_Survey__c();
		Test.startTest();
		try {
			RP_CaseSurveyController.saveSurvey(cs);
		}catch(Exception ex) {}

		Test.stopTest();
	}


	@testSetup
	static void setup() {
		Account acc = new Account(name='Test Acc', EC_Production_Version__c = '1234');
		insert acc;
		System.assertEquals('Test Acc', acc.Name);

		Contact con = new Contact(lastname='Test Con', AccountId=acc.Id, Email='TestEmail@gmail.com');
		insert con;
		System.assertEquals('Test Con', con.LastName);


		Id CaseRecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'Community'].Id;
		Case cs = new Case(
			Origin='Portal'
			, Priority='High'
			, Status='Closed'
			, Type='Incident'
			, Subject='Test Case'
			, Description='Test Case Description'
			, ContactId = con.Id
			, AccountId = acc.Id
			, RecordTypeId = CaseRecordTypeId
			, Case_Topic__c = 'Workforce Products'
			, Service_Area__c = 'Professional'
			, Service__c = 'Professional'
			, Request__c = 'Professional'
		);
		insert cs;
		System.assertEquals('Closed', cs.Status);

		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];


			User us1 = new User(
			Alias = 'test1234'
			, Email='testuser@testorg1.com'
			, EmailEncodingKey='UTF-8'
			, LastName='Testing1'
			, LanguageLocaleKey='en_US'
			, LocaleSidKey='en_US'
			, ProfileId = p.Id
			, TimeZoneSidKey='America/Los_Angeles'
			, UserName='guestusermanager@testorg.com'
			);
			insert us1;
			System.assertEquals('Testing1', us1.LastName);


			User us = new User(
			Alias = 'test123'
			, Email='testuser@testorg.com'
			, EmailEncodingKey='UTF-8'
			, LastName='Testing'
			, LanguageLocaleKey='en_US'
			, LocaleSidKey='en_US'
			, ProfileId = p.Id
			, TimeZoneSidKey='America/Los_Angeles'
			, UserName='guestuser@testorg.com'
			, ManagerId = us1.Id
			);
			insert us;
			System.assertEquals('Testing', us.LastName);

			Case_Survey__c csv = new Case_Survey__c(
			Case__c = cs.Id
			, Case_Subject__c = cs.Subject
			, Comment__c = 'Test Comment'
			, Contact__c = con.Id
			, Score__c = 26
			, Status__c = 'Generated'
			, Support_Agent__c = us.Id
			, Support_Agent_Manager__c = us1.Id
			, Taken_Date__c = System.Now()
			);
			insert csv;
			System.assertEquals('Test Comment', csv.Comment__c);
		}


	}