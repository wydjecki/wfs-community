@isTest 
private class RP_UtilityTest {

	@isTest static void standardUtilityTest() {
		RP_Utility.getAssignmentRuleId();
		RP_Utility.getPicklistValues('Case','Type');
		RP_Utility.getDependentPicklistValues('Case', 'Case_Topic__c', 'Service__c', 'Workforce Products');
		RP_Utility.getHelpTexts('Case', new List<String>{'Type'});
		RP_Utility.getHelpText('Case', 'Type');
		RP_Utility.getBaseUrl();
        System.assertNotEquals(null, RP_Utility.getAssignmentRuleId());
        System.assertNotEquals(null, RP_Utility.getPicklistValues('Case','Type'));
	}

	@isTest static void serviceLoginTest() {
		Test.setMock(WebServiceMock.class, new UtilityServiceLoginMock());
		Test.startTest();
		RP_Utility.getSessionId();
		Test.stopTest();
	}

	@isTest static void getPicklistValuesMapByRecordTypeTest() {
		Test.setMock(WebServiceMock.class, new UtilityServiceReadMetadataMock());
		Test.startTest();
		RP_Utility.getPicklistValuesMapByRecordType('object', 'record type', new List<String> {'field'});
		Test.stopTest();
	}


	@isTest static void getPicklistValuesByRecordTypeTest() {
		Test.setMock(WebServiceMock.class, new UtilityServiceReadMetadataMock());
		Test.startTest();
		RP_Utility.getPicklistValuesByRecordType('object', 'record type', 'field');
		Test.stopTest();
	}

	@isTest static void getDependentPicklistValuesByRecordTypeTest() {
		Test.setMock(WebServiceMock.class, new UtilityServiceReadMetadataMock());
		Test.startTest();
		RP_Utility.getDependentPicklistValuesByRecordType('object', 'record type', 'field', 'field value');
		Test.stopTest();
	}

	@testSetup static void setup() {
		List<Service_Setting__c> ss = new List<Service_Setting__c>();
		ss.add(new Service_Setting__c(
			Name = 'Base URL'
			, Value__c = 'https://www.salesforce.com'
		));
		ss.add(new Service_Setting__c(
			Name = 'User Name'
			, Value__c = 'integration'
		));
		ss.add(new Service_Setting__c(
			Name = 'Key'
			, Value__c = 'key-of-integration'
		));
		insert ss;
	}
}