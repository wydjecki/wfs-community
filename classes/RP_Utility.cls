public without sharing class RP_Utility {

    private static final Integer TIMEOUT_LIMIT = 120000;
	private static Map<String,Integer> hexMap = new Map<String, Integer>{'0'=>0,'1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9, 'A'=>10,'B'=>11,'C'=>12,'D'=>13,'E'=>14,'F'=>15,'a'=>10,'b'=>11,'c'=>12,'d'=>13,'e'=>14,'f'=>15};

	public static String getBaseUrl() {
		return Service_Setting__c.getInstance('Base URL').Value__c;
	}

	public static String getSessionId() {
		Map<String, Service_Setting__c> settings = Service_Setting__c.getAll();
		UtilityService.Soap service = new UtilityService.Soap();
		service.endpoint_x = getBaseUrl() + '/services/Soap/u/35.0';
		UtilityService.LoginResult result = service.login(settings.get('User Name').Value__c, settings.get('Key').Value__c);
		System.debug('RESULT: ' + result);
		return result.sessionId;
	}

    public static String getAssignmentRuleId() {
        Id arID;
        arID = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1].Id;
        System.debug('Assignment Rule ID : ' + arId);
        return arId;
    }

	public static Map<String, List<String>> getPicklistValuesMapByRecordType(String objectName, String recordTypeName, List<String> fieldNames) {
		Map<String, List<String>> valueMap = new Map<String, List<String>>();
		for (String fieldName : fieldNames) {
			valueMap.put(fieldName, new List<String>());
		}
        UtilityService.Port service = new UtilityService.Port();
		service.endpoint_x = getBaseUrl() +  '/services/Soap/m/35.0';
        service.SessionHeader = new UtilityService.SessionHeader_element();
        //System.debug('SESSION ID: ' + UserInfo.getSessionId());
        service.SessionHeader.sessionId = (!Test.isRunningTest()) ? getSessionId() : null;
        service.timeout_x = TIMEOUT_LIMIT;
        UtilityService.IReadResult result = service.readMetadata('RecordType', new String[] {
        	objectName + '.' + recordTypeName.replace(' ', '_')
        });
        System.debug('RESULT: ' + result);
        List<UtilityService.Metadata> mds = result.getRecords();
        System.debug('MDS: ' + mds);
        if (!mds.isEmpty()) {
        	UtilityService.RecordType rt = (UtilityService.RecordType) mds[0];
        	System.debug('RT: ' + rt.label + ' -- ' + rt);
        	List<UtilityService.RecordTypePicklistValue> picklists = rt.picklistValues;
        	System.debug('PICKLISTS: ' + picklists);
        	if (picklists != null) {
	        	for (UtilityService.RecordTypePicklistValue picklist : picklists) {
	        		if (valueMap.containsKey(picklist.picklist)) {
	        			List<String> values = valueMap.get(picklist.picklist);
	        			for (UtilityService.PicklistValue pv : picklist.values) {
	        				String value = EncodingUtil.urlDecode(pv.fullName, 'UTF-8');
	        				System.debug('NAME: ' + value);
	        				values.add(value);
	        			}
	        			valueMap.put(picklist.picklist, values);
	        		}
	        	}
        	}
        }
        return valueMap;
	}

	public static List<String> getPicklistValuesByRecordType(String objectName, String recordTypeName, String fieldName) {
		List<String> values = new List<String>();
        UtilityService.Port service = new UtilityService.Port();
		service.endpoint_x = getBaseUrl() +  '/services/Soap/m/35.0';
        service.SessionHeader = new UtilityService.SessionHeader_element();
        //System.debug('SESSION ID: ' + UserInfo.getSessionId());
        service.SessionHeader.sessionId = (!Test.isRunningTest()) ? getSessionId() : null;
        service.timeout_x = TIMEOUT_LIMIT;

        UtilityService.IReadResult result = service.readMetadata('RecordType', new String[] {
        	objectName + '.' + recordTypeName.replace(' ', '_')
        });
        System.debug('RESULT: ' + result);
        List<UtilityService.Metadata> mds = result.getRecords();
        System.debug('MDS: ' + mds);
        if (!mds.isEmpty()) {
        	UtilityService.RecordType rt = (UtilityService.RecordType) mds[0];
        	System.debug('RT: ' + rt.label + ' -- ' + rt);
        	List<UtilityService.RecordTypePicklistValue> picklists = rt.picklistValues;
        	System.debug('PICKLISTS: ' + picklists);
        	if (picklists != null) {
	        	for (UtilityService.RecordTypePicklistValue picklist : picklists) {
	        		//System.debug('PICKLIST: ' + picklist);
	        		if (picklist.picklist == fieldName) {
	        			System.debug('FOUND: ' + picklist);
	        			for (UtilityService.PicklistValue pv : picklist.values) {
	        				String value = EncodingUtil.urlDecode(pv.fullName, 'UTF-8');
	        				System.debug('NAME: ' + value);
	        				values.add(value);
	        			}
	        		}
	        	}
        	}
        }
        return values;

	}

	public static List<String> getDependentPicklistValuesByRecordType(String objectName, String recordTypeName, String fieldName, String fieldValue) {
		List<String> values = new List<String>();
        UtilityService.Port service = new UtilityService.Port();
		service.endpoint_x = getBaseUrl() +  '/services/Soap/m/35.0';
        service.SessionHeader = new UtilityService.SessionHeader_element();
        service.SessionHeader.sessionId = (!Test.isRunningTest()) ? getSessionId() : null;
        service.timeout_x = TIMEOUT_LIMIT;

        UtilityService.IReadResult result = service.readMetadata('RecordType', new String[] {
        	objectName + '.' + recordTypeName.replace(' ', '_')
        });
        System.debug('RESULT: ' + result);
        List<UtilityService.Metadata> mds = result.getRecords();
        System.debug('MDS: ' + mds);
        if (!mds.isEmpty()) {
        	UtilityService.RecordType rt = (UtilityService.RecordType) mds[0];
        	System.debug('RT: ' + rt.label + ' -- ' + rt);
        	List<UtilityService.RecordTypePicklistValue> picklists = rt.picklistValues;
        	System.debug('PICKLISTS: ' + picklists);
        	if (picklists != null) {

	        	Set<String> controlledValues = new Set<String>();
		        result = service.readMetadata('CustomField', new String[] {
		        	objectName + '.' + fieldName
		        });
		        System.debug('FIELD RESULT: ' + result);
				UtilityService.CustomField field = (UtilityService.CustomField) result.getRecords()[0];
				System.debug('FIELD: ' + field);
				if (field.picklist != null && field.picklist.picklistValues != null) {
					for (UtilityService.PicklistValue pv : field.picklist.picklistValues) {
						System.debug('PV: ' + pv.fullName + ' -- ' + pv.controllingFieldValues);
						if (pv.controllingFieldValues != null) {
							for (String cv : pv.controllingFieldValues) {
								if (cv == fieldValue) {
									controlledValues.add(pv.fullName);
								}
							}
						}
					}
				}
				System.debug('CONTROLLED VALUES: ' + controlledValues);

	        	for (UtilityService.RecordTypePicklistValue picklist : picklists) {
	        		//System.debug('PICKLIST: ' + picklist);
	        		if (picklist.picklist == fieldName) {
	        			System.debug('FOUND: ' + picklist);
	        			for (UtilityService.PicklistValue pv : picklist.values) {
	        				System.debug('NAME: ' + pv.fullName);
	        				if (controlledValues.contains(pv.fullName)) {
		        				String value = EncodingUtil.urlDecode(pv.fullName, 'UTF-8');
		        				System.debug('NAME: ' + value);
		        				values.add(value);
	        				}
	        			}
	        		}
	        	}
	        }
        }
        return values;

	}
	public static List<String> getPicklistValues(String objectName, String fieldName) {
		List<String> values = new List<String>();

		try {
			Schema.DescribeSObjectResult[] res = Schema.describeSObjects(new List<String> {objectName});
			Map<String, Schema.SObjectField> fieldMap = res[0].fields.getMap();
			Schema.DescribeFieldResult fres = fieldMap.get(fieldName).getDescribe();
			for (Schema.PicklistEntry pe : fres.getPicklistValues()) {
				values.add(pe.getValue());
			}
		} catch (Exception ex) {
			System.debug('getPicklistValues ERROR: ' + ex.getMessage());
		}
		//System.debug('PICKLIST: ' + objectName + ', ' + fieldName + ': ' + values);
		return values;
	}

	public static List<String> getDependentPicklistValues(String objectName, String controllingField, String dependentField, String controllingValue) {
		List<String> values = new List<String>();
		Schema.DescribeSObjectResult o = Schema.describeSObjects(new List<String> {objectName})[0];
		Schema.DescribeFieldResult f = o.fields.getMap().get(controllingField).getDescribe();
		Integer index = -1;
		Integer counter = 0;
		for (Schema.PicklistEntry pe : f.getPicklistValues()) {
			if (pe.value.toLowerCase() == controllingValue.toLowerCase()) {
				index = counter;
				//System.debug('VALUE: ' + pe.value + ' -- COUNTER: ' + index);
				break;
			}
			counter++;
		}
		if (index == -1) return values;

		f = o.fields.getMap().get(dependentField).getDescribe();
		for (Schema.PicklistEntry pe : f.getPicklistValues()) {
			String js = JSON.serialize(pe);
			Map<String, String> jmap = (Map<String, String>) JSON.deserialize(js, Map<String, String>.class);
			String validFor = jmap.get('validFor');
			//System.debug('FIELD: ' + jmap.get('value') + ' -- VALIDFOR: ' + validFor);
			if (String.isNotBlank(validFor) && isDependentValue(index, validFor)) values.add(jmap.get('value'));
			//System.debug('VALUES: ' + values);
		}

		return values;
	}

	public static Map<String, String> getHelpTexts(String objectName, List<String> fieldNames) {
		Map<String, String> textMap = new Map<String, String>();
		Schema.DescribeSObjectResult o = Schema.describeSObjects(new List<String> {objectName})[0];
		Map<String, Schema.SObjectField> fieldMap = o.fields.getMap();

		for (String fieldName : fieldNames) {
			if (fieldMap.containsKey(fieldName)) {
				Schema.DescribeFieldResult f = fieldMap.get(fieldName).getDescribe();
				textMap.put(fieldName, f.getInlineHelpText());
			}
		}
		return textMap;
	}

	public static String getHelpText(String objectName, String fieldName) {
		String text = '';
		Schema.DescribeSObjectResult o = Schema.describeSObjects(new List<String> {objectName})[0];
		Schema.DescribeFieldResult f = o.fields.getMap().get(fieldName).getDescribe();

		text = f.getInlineHelpText();

		return text;
	}



/**** PRIVATE METHODS *****/
	private static Boolean isDependentValue(Integer parentValueIndex, String validFor) {
		Boolean result = false;
		if (String.isBlank(validFor)) return result;
		String decoded = EncodingUtil.convertToHex(EncodingUtil.base64Decode(validFor));
		//System.debug('DECODED: ' + decoded);
		List<String> tokens = decoded.split('');
		//System.debug('TOKENS: ' + tokens);
		if (tokens.size() > 0) {
			Integer pos = parentValueIndex / 4;
			//System.debug('POS: ' + pos + ' TOKEN: ' + tokens[pos]);
			Integer value = hexMap.get(tokens[pos]);
			Integer idx = Integer.valueOf(Math.mod(parentValueIndex, 4));
			//System.debug('VAL: ' + value + ' - IDX: ' + idx);
			result = ((value & (8 >> idx)) > 0);
			//System.debug('RES: ' + result);
		}

		return result;
	}

/*
	public static String getSessionId2() {
		PartnerService.Soap service = new PartnerService.Soap();
		service.endpoint_x = 'https://workforcesoftware--service.cs30.my.salesforce.com/services/Soap/u/39.0';
		PartnerService.LoginResult result = service.login('awidjaja@workforcesoftware.com.service', 'Op3nWFS!tMfKz3O6TwEed8UxcJw8Jv0L1');
		System.debug('RESULT: ' + result);
		return result.sessionId;
	}

	public static List<String> getPicklistValuesByRecordType2(String objectName, String recordTypeName, String fieldName) {
		List<String> values = new List<String>();
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        //System.debug('SESSION ID: ' + UserInfo.getSessionId());
        service.SessionHeader.sessionId = getSessionId();
        service.timeout_x = TIMEOUT_LIMIT;

        MetadataService.IReadResult result = service.readMetadata('RecordType', new String[] {
        	objectName + '.' + recordTypeName.replace(' ', '_')
        });
        System.debug('RESULT: ' + result);
        List<MetadataService.Metadata> mds = result.getRecords();
        System.debug('MDS: ' + mds);
        if (!mds.isEmpty()) {
        	MetadataService.RecordType rt = (MetadataService.RecordType) mds[0];
        	System.debug('RT: ' + rt.label + ' -- ' + rt);
        	List<MetadataService.RecordTypePicklistValue> picklists = rt.picklistValues;
        	System.debug('PICKLISTS: ' + picklists);
        	if (picklists != null) {
	        	for (MetadataService.RecordTypePicklistValue picklist : picklists) {
	        		//System.debug('PICKLIST: ' + picklist);
	        		if (picklist.picklist == fieldName) {
	        			System.debug('FOUND: ' + picklist);
	        			for (MetadataService.PicklistValue pv : picklist.values) {
	        				System.debug('NAME: ' + pv.fullName);
	        				values.add(pv.fullName);
	        			}
	        		}
	        	}
        	}
        }
        return values;

	}

	public static List<String> getDependentPicklistValuesByRecordType2(String objectName, String recordTypeName, String fieldName, String fieldValue) {
		List<String> values = new List<String>();
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        System.debug('SESSION ID: ' + UserInfo.getSessionId());
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        service.timeout_x = TIMEOUT_LIMIT;

        MetadataService.IReadResult result = service.readMetadata('RecordType', new String[] {
        	objectName + '.' + recordTypeName.replace(' ', '_')
        });
        System.debug('RESULT: ' + result);
        List<MetadataService.Metadata> mds = result.getRecords();
        System.debug('MDS: ' + mds);
        if (!mds.isEmpty()) {
        	MetadataService.RecordType rt = (MetadataService.RecordType) mds[0];
        	System.debug('RT: ' + rt.label + ' -- ' + rt);
        	List<MetadataService.RecordTypePicklistValue> picklists = rt.picklistValues;
        	System.debug('PICKLISTS: ' + picklists);
        	if (picklists != null) {

	        	Set<String> controlledValues = new Set<String>();
		        result = service.readMetadata('CustomField', new String[] {
		        	objectName + '.' + fieldName
		        });
		        System.debug('FIELD RESULT: ' + result);
				MetadataService.CustomField field = (MetadataService.CustomField) result.getRecords()[0];
				System.debug('FIELD: ' + field);
				if (field.picklist != null && field.picklist.picklistValues != null) {
					for (MetadataService.PicklistValue pv : field.picklist.picklistValues) {
						System.debug('PV: ' + pv.fullName + ' -- ' + pv.controllingFieldValues);
						if (pv.controllingFieldValues != null) {
							for (String cv : pv.controllingFieldValues) {
								if (cv == fieldValue) {
									controlledValues.add(pv.fullName);
								}
							}
						}
					}
				}
				System.debug('CONTROLLED VALUES: ' + controlledValues);

	        	for (MetadataService.RecordTypePicklistValue picklist : picklists) {
	        		//System.debug('PICKLIST: ' + picklist);
	        		if (picklist.picklist == fieldName) {
	        			System.debug('FOUND: ' + picklist);
	        			for (MetadataService.PicklistValue pv : picklist.values) {
	        				System.debug('NAME: ' + pv.fullName);
	        				if (controlledValues.contains(pv.fullName)) values.add(pv.fullName);
	        			}
	        		}
	        	}
	        }
        }
        return values;

	}
*/


}