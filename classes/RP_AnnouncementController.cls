public class RP_AnnouncementController {

    @AuraEnabled
	public static String currentUserType() {
		String userType;
		Id partner = [Select Id, name FROM Profile Where Name = 'Partner Community Plus Admin User'].Id;
		Id customer = [Select Id, name FROM Profile Where Name = 'Customer Community Plus Login User - Custom'].Id;
		Id superCustomer = [Select Id, name FROM Profile Where Name = 'Customer Community Plus Login Admin User - Custom'].Id;

		if(UserInfo.getProfileId() == partner) {
			userType = 'Partner';
		} else if(UserInfo.getProfileId() == customer || UserInfo.getProfileId() == superCustomer) {
			userType = 'Customer';
		} else{
			userType = '';
		}
		return userType;
	}

    @AuraEnabled
	public static Announcement__c[] getAnnouncements(String currentUser) {
		String soql  =
		'SELECT Id'
			+ ', Announcement_Message__c, Name__c'
			+ ', Start_Time__c, End_Time__c, Type__c'
			+ ', Partner_Only__c, Customer_Only__c'
			+ ' FROM Announcement__c'
		;

        DateTime systemNow = System.now();
        // String displayTime = gmtTime.format('MM/dd/yyy HH:mm:ss', 'America/New_York');

        String andTimeAndPriority =
        ' AND Start_Time__c <= :systemNow'
            + ' AND End_Time__c >= :systemNow'
            + ' ORDER BY Priority__c'
        ;

		String wherePartner = ' WHERE Customer_Only__c != true';
			soql = ( currentUser == 'Partner' ) ? ( soql += wherePartner ) : soql;

		String whereCustomer = ' WHERE Partner_Only__c != true';
			soql = ( currentUser == 'Customer' ) ? ( soql += whereCustomer ) : soql;

        String whereTimeAndPriority = andTimeAndPriority.replaceFirst( ' AND', ' WHERE' );
            soql = String.isBlank(currentUser) ? ( soql += whereTimeAndPriority ) : ( soql += andTimeAndPriority );

        System.debug('SOQL : ' + soql);
		return Database.query(soql);
	}
}