public with sharing class RP_DocumentController {
	private static final String GUID_DUMMY = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';
	private static User currentUser;

	static {
		currentUser = [
			SELECT Id, Name, ContactId
			, Contact.AccountId
			, Contact.Account.Document_Access__c
			, Contact.Account.Ecosystem__c
			, Contact.Account.Product_Family__c
			FROM User
			WHERE Id = :UserInfo.getUserId()
		];

	}

	@AuraEnabled
	public static User getCurrentUser() {
		return currentUser;
	}

	@AuraEnabled
	public static Topic getTopics() {
		Topic t = new Topic();
		System.debug('GET TOPICS NEW');
		String result = RP_DocumentService.getTopics();
		System.debug('USER: ' + currentUser);
		if (String.isNotBlank(result)
			&& currentUser.ContactId != null 
			&& currentUser.Contact.AccountId != null
			&& String.isNotBlank(currentUser.Contact.Account.Document_Access__c)
		) {
			Boolean isPartner = false;
			Boolean isCustomer = false;

			List<String> types = currentUser.Contact.Account.Document_Access__c.split(';');
			for (String type : types) {
				if (type == 'Customer') isCustomer = true;
				else isPartner = true;
			}

			t = (Topic) JSON.deserialize(result, Topic.class);
			System.debug('TOPIC: ' + t);
			if (t.Children != null) { //top level
				for (Integer i = t.Children.size()-1; i >= 0; i--) {
					System.debug('LEVEL 1: ' + t.Children[i].Name + ' -- ' + t.Children[i].CategoryTargetAudience);
					if (t.Children[i].CategoryTargetAudience == null) t.Children.remove(i);
					else if (
						(isCustomer && !t.Children[i].CategoryTargetAudience.contains('Customer')) 
						|| (isPartner && !t.Children[i].CategoryTargetAudience.contains('Partner'))
					) t.Children.remove(i);
					else if (t.Children[i].Children != null) {
						for (Integer j = t.Children[i].Children.size()-1; j >= 0; j--) {
							System.debug('		LEVEL 2: ' + t.Children[i].Children[j].Name + ' -- ' + t.Children[i].Children[j].CategoryTargetAudience);
							if (t.Children[i].Children[j].CategoryTargetAudience == null) t.Children[i].Children.remove(j);
							else if (
								(isCustomer && !t.Children[i].Children[j].CategoryTargetAudience.contains('Customer')) 
								|| (isPartner && !t.Children[i].Children[j].CategoryTargetAudience.contains('Partner'))
							) t.Children[i].Children.remove(j);
						}
					}
				}
			}

		}
		System.debug('NEW TOPICS: ' + t);
		return t;
	}
/*
	@AuraEnabled
	public static String getTopicsOld() {
		System.debug('GET TOPICS');
		return RP_DocumentService.getTopics();
	}
*/
	@AuraEnabled
	public static String search(String filter) {
		System.debug('FILTER: ' + filter);
		SearchFilter f = (SearchFilter) JSON.deserialize(filter, SearchFilter.class);
		System.debug('F: ' + f);

		//System.debug('SEARCH DOCS: ' + keyword + ' -- START ROW: ' + startRow + ' -- ROW LIMIT: ' + rowLimit);
		String access = buildAccessRequest();
		String refinements = buildRefinementRequest(f.refinements);

		System.debug('ACCESS: ' + access);
		return RP_DocumentService.search(
			(f.topicInfo != null) ? f.topicInfo.get('Id') : ''
			, f.keyword
			, buildAccessRequest()
			, refinements
			, Integer.valueOf(f.pageInfo.get('StartRow'))
			, Integer.valueOf(f.pageInfo.get('RowLimit'))
		);
	}

    @AuraEnabled
    public static String getGuestAccessLink(String path) {
       return RP_DocumentService.getLink(path);
    }
    
	@AuraEnabled
	public static String downloadFile(String path) {
		return RP_DocumentService.getFile(path);
	}

	private static String buildRefinementRequest(Map<String, List<String>> refinementMap) {
		String result = '';
		if (refinementMap != null && refinementMap.size() > 0) {
			List<String> refinements = new List<String>();
			String refinement;
			for (String key : refinementMap.keyset()) {
				List<String> values = refinementMap.get(key);
				if (key == 'Product') refinement = 'RefinableCommunityProduct:';
				else if (key == 'Topic') refinement = 'RefinableCommunityPortalCategory:';
				else if (key == 'Audience') refinement = 'RefinableCommunityAudience:';
				else if (key == 'FileType') refinement = 'FileType:';
				else if (key == 'ModifiedDate') refinement = 'LastModifiedTime:';

				if (values != null && values.size() > 0) {
					if (values.size() == 1) refinements.add(refinement + values[0]);
					else refinements.add(refinement + + 'and(' + String.join(values, ',') + ')');
				}
			}
			if (refinements.size() == 1) result = refinements[0];
			else result = 'and(' + String.join(refinements, ',') + ')';
		}
		System.debug('REFINEMENT FILTERS: ' + result);
		return result;
	}

	private static String buildAccessRequest() {
		String result = '';

		if (currentUser.ContactId == null) {
			// employee -- DO NOTHING FOR NOW
		} else if (currentUser.Contact.AccountId == null) {
			//something is wrong: contact without account!
		} else {
			String accessGroup;
			List<String> accessGroups = new List<String>();

			//Document Access
			String userAccess = currentUser.Contact.Account.Document_Access__c;
			if (String.isNotBlank(userAccess)) {
				accessGroup = splitValueByGroup('RefinableWhoCanView', userAccess);
				if (String.isNotBlank(accessGroup)) accessGroups.add(accessGroup);

				if (accessGroup.contains('Customer')) {
					String productAccess = getProductGUID(currentUser.Contact.Account.Product_Family__c);
					accessGroup = splitValueByGroup('AllCommunityProduct', productAccess);
					if (String.isNotBlank(accessGroup)) accessGroups.add(accessGroup);
				}

				String ecosystem = currentUser.Contact.Account.Ecosystem__c;
				ecosystem = 'WorkForce;' + (String.isNotBlank(ecosystem) ? ecosystem : '');
				accessGroup = splitValueByGroup('RefinablePartnerEcosystem', ecosystem);
				if (String.isNotBlank(accessGroup)) accessGroups.add(accessGroup);
			}
			result = String.join(accessGroups, ' AND ');
		} 
		System.debug('ACCESS GROUPS: ' + result);

		return result;
	}
	
	private static String splitValueByGroup(String groupName, String value) {
		String result = '';
		if (String.isNotBlank(groupName) && String.isNotBlank(value)) {
			List<String> groups = new List<String>();
			List<String> tokens = value.split(';');
			for (String token : tokens) {
				groups.add(groupName + ':' + token);
			}
			result = '(' + String.join(groups, ' OR ') + ')';
		}
		return result;
	}

	private static String getProductGUID(String productList) {
		String result = '';
		List<String> guids = new List<String>();
		System.debug('PRODUCT LIST: ' + productList);
		if (String.isNotBlank(productList)) {
			Map<String, String> guidByProduct = getGUIDByProduct();
			List<String> products = productList.split(';');
			for (String product : products) {
				System.debug('PRODUCT: ' + product);
				if (guidByProduct.containsKey(product)) {
					guids.add(guidByProduct.get(product));
				}
			}
		}

		if (guids.isEmpty()) guids.add(GUID_DUMMY);
		result = String.join(guids, ';');

		return result;
	}

	private static Map<String, String> getGUIDByProduct() {
		Map<String, String> productMap = new Map<String, String>();

		try {
			String productResponse = RP_DocumentService.getProducts();
			Map<String, Object> rootMap = (Map<String, Object>) JSON.deserializeUntyped(productResponse);
			//System.debug('ROOT MAP: ' + rootMap);
			List<Object> children  = (List<Object>) rootMap.get('Children');
			//System.debug('CHILDREN: ' + children);
			for (Object child : children) {
				Map<String, Object> childMap = (Map<String, Object>) child;
				//System.debug('CHILD MAP: ' + childMap);
				productMap.put((String) childMap.get('Name'), (String) childMap.get('Id'));
			}
		} catch (Exception ex) {
			System.debug('FAILED TO GET PRODUCT GUID: ' + ex.getMessage());
		}

		System.debug('PRODUCT MAP: ' + productMap);
		return productMap;
	}

	public class Topic {
		@AuraEnabled public String Id;
		@AuraEnabled public String Name;
		@AuraEnabled public String CustomDescription;
		@AuraEnabled public String CategoryTargetAudience;
		@AuraEnabled public List<Topic> Children;
	}

	public class SearchFilter {
		@AuraEnabled public String keyword;
		@AuraEnabled public Map<String, List<String>> refinements;
		@AuraEnabled public Map<String, String> topicInfo;
		@AuraEnabled public Map<String, String> pageInfo;
	}

/*

	@AuraEnabled
	public static String searchDocuments(String topicId, String keyword, String filters, Integer startRow, Integer rowLimit) {
		System.debug('SEARCH DOCS: ' + keyword + ' -- START ROW: ' + startRow + ' -- ROW LIMIT: ' + rowLimit);
		String access = buildAccessRequest();
		System.debug('ACCESS: ' + access);
		return RP_DocumentService.search(topicId, keyword, access, filters, Integer.valueOf(startRow), Integer.valueOf(rowLimit));
	}



	@AuraEnabled 
	public static SearchFilter getSearchFilter() {
		SearchFilter f = new SearchFilter();
		f.SelectProperties = new ResultsArray();
		f.SelectProperties.results = new List<String>();
		f.RefinementFilters = new ResultsArray();
		f.RefinementFilters.results = new List<String>();
		return f;
	}

	@AuraEnabled
	public static String getFilterJson() {
		SearchFilter f = new SearchFilter();
		f.SelectProperties = new ResultsArray();
		f.SelectProperties.results = new List<String> {'Title', 'Author'};
		f.Culture = '1044';
		f.RefinementFilters = new ResultsArray();
		f.RefinementFilters.results = new List<String> {'fileExtension:equals'};
		return JSON.serialize(f);
	}
*/
	/*
		if Partner, pass the Ecosystem
		if Customer, pass the Product

	private static String getConstraintByUser() {
		String constraint = '';

		if (currentUser.ContactId == null) {
			// employee
		} else if (currentUser.Contact.AccountId == null) {
			//something is wrong!
		} else if (String.isNotBlank(currentUser.Contact.Account.Partner_Type__c)) {
			//partner

			List<String> types;
			String partnerType = currentUser.Contact.Account.Partner_Type__c;
			if (partnerType.contains(';')) {
				types = partnerType.split(';');
			} else types = new List<String> {partnerType};

			constraint = '(RefinableWhoCanView:';
			if (types.size() == 1) {
				constraint += types[0];
			} else {
				constraint += String.join(types, ' OR RefinableWhoCanView:');
			}
			constraint += ')';

			if (String.isNotBlank(currentUser.Contact.Account.Ecosystem__c)) {
				List<String> ecos;
				String partnerEcosystem = currentUser.Contact.Account.Ecosystem__c;
				if (partnerEcosystem.contains(';')) {
					ecos = partnerEcosystem.split(';');
				} else {
					ecos = new List<String> {partnerEcosystem};
				}
				constraint += ' AND (RefinablePartnerEcosystem:';
				if (ecos.size() == 1) {
					constraint += ecos[0];
				} else {
					constraint += String.join(ecos, ' OR RefinablePartnerEcosystem:');
				}
				constraint += ')';
			}

		} else {
			//customer
			//constraint = 'RefinableCommunityAudience:Customer';
			constraint = 'RefinableWhoCanView:Customer'; //FIX THIS LATER!
		}
		System.debug('CONSTRAINT:' + constraint);
		return constraint;
	}

	private static String getConstraintByUser2(){

		String constraint = '';

		if (currentUser.ContactId == null) {
			// employee
		} else if (currentUser.Contact.AccountId == null) {
			//something is wrong!
		} else if (String.isNotBlank(currentUser.Contact.Account.Partner_Type__c)) {
			// we have identified the user as a 'Partner'
			// boo apex static classes, could probably abstract this utility class to accept a user instead of these strings
			// get the Partner Type constraints string AKA WhoCanView
			string partnerTypeConstraints = UserHiddenConstraintUtility.createPartnerTypeConstraints(currentUser.Contact.Account.Partner_Type__c);
			// get the ecosystem constraints string AKA RefinablePartnerEcosystem
			string partnerEcosystemConstraints = UserHiddenConstraintUtility.createPartnerEcosystemConstraint(currentUser.Contact.Account.Ecosystem__c);

			// combine the two values with a third call
			string fullPartnerConstraint = UserHiddenConstraintUtility.createFullPartnerConstraint(partnerEcosystemConstraints, partnerTypeConstraints);
			constraint = fullPartnerConstraint;
			
		} else {
			// the user is a 'Customer'
			//constraint = 'RefinableCommunityAudience:Customer';
			constraint = 'RefinableCommunityAudience:Partner'; //FIX THIS LATER!
		}
		System.debug('CONSTRAINT' + constraint);
		return constraint;
	}

	private static String getHardCodedJohnDoeConstraints(){

		return '(RefinableWhoCanView:Reseller OR RefinableWhoCanView:Support) AND (RefinablePartnerEcosystem:SAP OR RefinablePartnerEcosystem:Oracle OR RefinablePartnerEcosystem:Cerner) AND ContentTypeId:0x01010045287B932D1C4739A0C406ADC0B4048A01*';

	}
*/

/*
	public class SearchFilter {
		@AuraEnabled public ResultsArray SelectProperties;
		@AuraEnabled public String Culture;
		@AuraEnabled public ResultsArray RefinementFilters;
		@AuraEnabled public ResultsArray Refiners;
	}

	public class ResultsArray {
		@AuraEnabled public List<String> results;
	}
*/
}