@isTest private class RP_DocumentServiceTest {
	@isTest static void bearerTokenTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentService.getBearerToken();
		Test.stopTest();
	}

	@isTest static void topicTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentService.getTopics();
		Test.stopTest();
	}

	@isTest static void productTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentService.getProducts();
		Test.stopTest();
	}

	@isTest static void fileTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentService.getFile('');
		Test.stopTest();
	}

	@isTest static void linkTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentService.getLink('');
		Test.stopTest();
	}

	@isTest static void searchTest() {
		Test.setMock(HttpCalloutMock.class, new RP_DocumentServiceMock.HttpCallout());
		Test.startTest();
		RP_DocumentService.search('', '', '', '', 0, 1);
		Test.stopTest();
	}


	@testSetup static void setup() {
		insert new Community_Setting__c(
			SetupOwnerId = UserInfo.getOrganizationId()
			, Document_Topic_URL__c = 'https://document.topic.url'
			, Document_Product_URL__c = 'https://document.product.url'
			, Document_Base_Search_URL__c = 'https://document.base.search.url'
			, Document_Search_Refiners__c = 'RefinableCommunityAudience'
			, Document_Content_Type__c = '0x29302932039'
			, Document_Data_Set_GUID__c = '2492042-2492409-20492049'
		);
	}
}